START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(003,'UP','Add_Content_Type',''); COMMIT;
alter table hm_channel add column `content_type` varchar(12) DEFAULT NULL COMMENT 'stars, games';
DELIMITER //
CREATE DEFINER=`root`@`%` TRIGGER `update_content_type` BEFORE UPDATE ON `hm_channel` FOR EACH ROW BEGIN
    IF new.gid <> 124
    THEN
        SET NEW.content_type = 'stars';
    ELSE
        SET NEW.content_type = 'games';
    END IF;
END; //
DELIMITER ;

update hm_channel set content_type = 'stars' where gid <> 124;
update hm_channel set content_type = 'games' where gid = 124;
