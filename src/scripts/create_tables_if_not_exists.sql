CREATE TABLE IF NOT EXISTS `database_version` (
    `id` int NOT NULL AUTO_INCREMENT,
    `version` int(11) NOT NULL,
    `migration_type` varchar(5) NOT NULL COMMENT 'UP OR DOWN',
    `description` varchar(255) NOT NULL COMMENT 'description for the database upgrade',
    `reason` varchar(255) NOT NULL COMMENT 'reason for migration',
    `created_at` timestamp DEFAULT CURRENT_TIMESTAMP COMMENT 'timestamp for the db migration',
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `migration_lock` (
    `ip` varchar(40) NOT NULL COMMENT 'the ip that holds this lock, it could ipv6',
    `hostname` varchar(255) NOT NULL COMMENT 'UP OR DOWN',
    `key` varchar(36) NOT NULL COMMENT 'key for the migration lock only the correct key should unlock the lock',
    `created_at` timestamp DEFAULT CURRENT_TIMESTAMP COMMENT 'timestamp for the db migration'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
