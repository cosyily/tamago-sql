db_update_label:BEGIN
    LOCK TABLES database_version WRITE;
    SET @version = $version;
    SET @db_version = select version from database_version order by created_at desc limit 1;
    IF @@ROWCOUNT = 0
    BEGIN
        SET @db_version=0;
    END
    IF @version = @db_version THEN
    BEGIN
        LEAVE db_update_label;
    END
    ELSEIF @version > @db_version THEN
    BEGIN
        SET @migration_type='UP'
    END
    ELSE
    BEGIN
        SET @migration_type='DOWN'
    END

    SET @migration_type = $migration_type; 
    SET @description = '$description';
    SET @reason = '$reason';
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    START TRANSACTION;
    insert into database_version (version,migration_type,description,reason) values (@version,@migration_type,@description,@reason);
    


    IF `_rollback` THEN
        ROLLBACK;
    ELSE
        COMMIT;
    END IF;
    UNLOCK TABLES;
END
