START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(008,'UP','100_lengends',''); COMMIT;
CREATE TABLE `tmg_hundred_legend` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `game_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_game_type` (`game_type`),
  KEY `index_uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
