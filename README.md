WARNING!!! DO NOT TOUCH ANYTHING IN SRC


DEPENDENCIES:
mysql
mysqlconnectorpython
mysqlutilities

USAGE:
Step 1: 
cd databases;

Step 2: Create database if not yet created
run ./createDatabase.sh <database_name>;

Step 3: Configure endpoints
cd <database_name> and configure database.config

Step 4: Create Intial Migration
#generate_migration_repo_to_dev.sh <migration_description>
generate_migration_repo_to_dev.sh Initial

Step 5: Sync With Dev DB
#this will sync the db version with the repo version


Step 6: Deploy to Production
#this will deploy the repo version to the prod version
