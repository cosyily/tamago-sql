START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(001,'UP','Initial',''); COMMIT;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-error.tamago.db    Database: hm_small_game
-- ------------------------------------------------------
-- Server version	5.6.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_small_game`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_small_game` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_small_game`;

--
-- Table structure for table `small_game_black_list`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_black_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户下注黑名单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_item_maps`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_item_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `md5` varchar(32) NOT NULL,
  `item_maps` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `md5` (`md5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_order_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_order_2017_10` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period` bigint(15) unsigned NOT NULL,
  `game` varchar(25) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('win','loss','waiting','rollback','cancel') NOT NULL,
  `amount` float NOT NULL COMMENT '下注金额',
  `item` varchar(30) NOT NULL COMMENT '下注对象',
  `item_cname` varchar(25) NOT NULL,
  `count` int(11) unsigned NOT NULL COMMENT '下注数量',
  `peilv` float NOT NULL COMMENT '赔率',
  `loss_money` float NOT NULL COMMENT '应赔',
  `win_money` float NOT NULL COMMENT '赢得',
  `play_room_number` int(10) unsigned NOT NULL COMMENT '下注房间',
  `create_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `period` (`period`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_order_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_order_2017_11` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period` bigint(15) unsigned NOT NULL,
  `game` varchar(25) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('win','loss','waiting','rollback','cancel') NOT NULL,
  `amount` float NOT NULL COMMENT '下注金额',
  `item` varchar(30) NOT NULL COMMENT '下注对象',
  `item_cname` varchar(25) NOT NULL,
  `count` int(11) unsigned NOT NULL COMMENT '下注数量',
  `peilv` float NOT NULL COMMENT '赔率',
  `loss_money` float NOT NULL COMMENT '应赔',
  `win_money` float NOT NULL COMMENT '赢得',
  `play_room_number` int(10) unsigned NOT NULL COMMENT '下注房间',
  `create_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `period` (`period`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_order_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_order_2017_8` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period` bigint(10) unsigned NOT NULL,
  `game` varchar(25) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('win','loss','waiting','rollback','cancel') NOT NULL,
  `amount` float NOT NULL COMMENT '下注金额',
  `item` varchar(30) NOT NULL COMMENT '下注对象',
  `item_cname` varchar(25) NOT NULL,
  `count` int(11) unsigned NOT NULL COMMENT '下注数量',
  `peilv` float NOT NULL COMMENT '赔率',
  `loss_money` float NOT NULL COMMENT '应赔',
  `win_money` float NOT NULL COMMENT '赢得',
  `play_room_number` int(10) unsigned NOT NULL COMMENT '下注房间',
  `create_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `period` (`period`),
  KEY `user_id` (`game`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_order_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_order_2017_9` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period` bigint(10) unsigned NOT NULL,
  `game` varchar(25) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('win','loss','waiting','rollback','cancel') NOT NULL,
  `amount` float NOT NULL COMMENT '下注金额',
  `item` varchar(30) NOT NULL COMMENT '下注对象',
  `item_cname` varchar(25) NOT NULL,
  `count` int(11) unsigned NOT NULL COMMENT '下注数量',
  `peilv` float NOT NULL COMMENT '赔率',
  `loss_money` float NOT NULL COMMENT '应赔',
  `win_money` float NOT NULL COMMENT '赢得',
  `play_room_number` int(10) unsigned NOT NULL COMMENT '下注房间',
  `create_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `period` (`period`),
  KEY `user_id` (`game`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_period_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_period_2017_10` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period` bigint(15) unsigned DEFAULT NULL COMMENT '期号',
  `game` varchar(25) NOT NULL,
  `start_time` int(10) unsigned NOT NULL,
  `end_time` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0：下注中，1：已结算，2：结算失败，3：已回滚，4：回滚失败',
  `user_count` int(10) unsigned NOT NULL,
  `flow_in` float NOT NULL COMMENT '平台流入',
  `flow_out` float NOT NULL COMMENT '平台流出',
  `win_items` text NOT NULL COMMENT '该期中奖项',
  `item_maps` varchar(32) NOT NULL COMMENT '物品信息、物品赔率等',
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `period` (`period`,`game`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='场次表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_period_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_period_2017_11` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period` bigint(15) unsigned DEFAULT NULL COMMENT '期号',
  `game` varchar(25) NOT NULL,
  `start_time` int(10) unsigned NOT NULL,
  `end_time` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0：下注中，1：已结算，2：结算失败，3：已回滚，4：回滚失败',
  `user_count` int(10) unsigned NOT NULL,
  `flow_in` float NOT NULL COMMENT '平台流入',
  `flow_out` float NOT NULL COMMENT '平台流出',
  `win_items` text NOT NULL COMMENT '该期中奖项',
  `item_maps` varchar(32) NOT NULL COMMENT '物品信息、物品赔率等',
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `period` (`period`,`game`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='场次表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_period_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_period_2017_8` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period` bigint(10) unsigned DEFAULT NULL COMMENT '期号',
  `game` varchar(25) NOT NULL,
  `start_time` int(10) unsigned NOT NULL,
  `end_time` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0：下注中，1：已结算，2：结算失败，3：已回滚，4：回滚失败',
  `user_count` int(10) unsigned NOT NULL,
  `flow_in` float NOT NULL COMMENT '平台流入',
  `flow_out` float NOT NULL COMMENT '平台流出',
  `win_items` text NOT NULL COMMENT '该期中奖项',
  `item_maps` varchar(32) NOT NULL COMMENT '物品信息、物品赔率等',
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `period` (`period`,`game`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='场次表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_period_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_period_2017_9` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period` bigint(10) unsigned DEFAULT NULL COMMENT '期号',
  `game` varchar(25) NOT NULL,
  `start_time` int(10) unsigned NOT NULL,
  `end_time` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0：下注中，1：已结算，2：结算失败，3：已回滚，4：回滚失败',
  `user_count` int(10) unsigned NOT NULL,
  `flow_in` float NOT NULL COMMENT '平台流入',
  `flow_out` float NOT NULL COMMENT '平台流出',
  `win_items` text NOT NULL COMMENT '该期中奖项',
  `item_maps` varchar(32) NOT NULL COMMENT '物品信息、物品赔率等',
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `period` (`period`,`game`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='场次表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_rooms`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL,
  `room_number` int(10) unsigned NOT NULL,
  `gid` int(10) unsigned NOT NULL,
  `games` bigint(20) NOT NULL,
  `admin_id` int(10) unsigned NOT NULL,
  `author` varchar(10) NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cid` (`cid`),
  KEY `gid` (`gid`,`games`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间游戏配置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_setting`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `game` varchar(25) NOT NULL,
  `is_open` enum('open','close') NOT NULL,
  `web_show` tinyint(4) NOT NULL COMMENT '0：不显示，1：所有用户，2：注册用户，3：绑定手机用户，4：充值用户',
  `mobile_show` tinyint(4) NOT NULL COMMENT '0：不显示，1：所有用户，2：注册用户，3：绑定手机用户，4：充值用户',
  `ctrl` varchar(255) NOT NULL COMMENT '分控配置',
  `single_game_max` int(11) unsigned NOT NULL,
  `single_money_limit` int(11) unsigned NOT NULL,
  `author` varchar(10) NOT NULL,
  `admin_id` int(11) unsigned NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='小游戏配置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `small_game_user_summary`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `small_game_user_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `game` varchar(25) NOT NULL COMMENT '游戏',
  `bet_count` int(11) unsigned NOT NULL,
  `win_count` int(11) unsigned NOT NULL,
  `loss_money` float NOT NULL,
  `win_money` float NOT NULL,
  `last_play_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`,`game`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='小游戏用户统计';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-01 16:11:31
