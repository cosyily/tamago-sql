START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(001,'UP','Initial',''); COMMIT;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-user.tamago.db    Database: tmg_users
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `tmg_users`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `tmg_users` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tmg_users`;

--
-- Table structure for table `facebook_user_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_0` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_1` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_10` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_11` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_12` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_13` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_14` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_15` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_16` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_17` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_18` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_19` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_2` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_20` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_21` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_22` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_23` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_24` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_25` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_26` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_27` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_28` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_29` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_3` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_30` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_31` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_4` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_5` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_6` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_7` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_8` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_user_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user_9` (
  `facebookid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`facebookid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_0` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_1` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_10` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_11` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_12` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_13` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_14` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_15` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_16` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_17` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_18` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_19` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_2` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_20` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_21` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_22` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_23` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_24` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_25` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_26` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_27` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_28` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_29` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_3` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_30` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_31` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_4` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_5` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_6` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_7` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_8` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `google_user_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user_9` (
  `googleid` varchar(25) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL COMMENT 'tmguid',
  PRIMARY KEY (`googleid`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_0` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_1` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_10` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_11` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_12` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_13` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_14` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_15` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_16` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_17` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_18` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_19` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_2` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_20` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_21` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_22` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_23` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_24` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_25` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_26` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_27` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_28` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_29` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_3` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_30` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_31` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_4` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_5` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_6` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_7` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_8` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mobile_user_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_user_9` (
  `uid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `area` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`mobile`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_user_achievement`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_user_achievement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(56) CHARACTER SET utf8 NOT NULL,
  `uid` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image_url` text CHARACTER SET utf8 NOT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uid`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uid` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'uid',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=337912 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_0` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_1` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_10` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_11` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_12` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_13` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_14` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_15` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_16` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_17` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_18` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_19` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_2` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_20` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_21` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_22` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_23` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_24` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_25` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_26` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_27` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_28` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_29` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_3` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_30` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_31` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_4` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_5` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_6` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_7` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_8` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info_9` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT 'password',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_password_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_password_0` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_password_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_password_1` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_1` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_10` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_11` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_12` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_13` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_14` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_15` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_16` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_17` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_18` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_19` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_2` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_20` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_21` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_22` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_23` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_24` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_25` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_26` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_27` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_28` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_29` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_3` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_30` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_31` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_32`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_32` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_33`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_33` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_34`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_34` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_35`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_35` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_36`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_36` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_37`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_37` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_38`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_38` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_39`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_39` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_4` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_40`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_40` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_41`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_41` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_42`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_42` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_43`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_43` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_44`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_44` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_45`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_45` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_46`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_46` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_47`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_47` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_48`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_48` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_49`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_49` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_5` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_50`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_50` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_6` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_7` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_8` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userbase_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbase_9` (
  `uid` int(11) DEFAULT NULL,
  `name` char(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `facebookid` bigint(25) unsigned DEFAULT NULL,
  `googleid` bigint(25) unsigned DEFAULT NULL,
  `email_activate_stat` tinyint(1) DEFAULT '0' COMMENT '邮箱状态：1：表示激活：0：表示未激活',
  `send_freebean` bigint(15) DEFAULT '0' COMMENT '默认送出的仙豆值',
  `get_experience` bigint(15) DEFAULT '0' COMMENT '获得的经验值',
  `anchor_experience` int(11) NOT NULL DEFAULT '0' COMMENT '主播仙能',
  `mobileareacode` varchar(4) DEFAULT NULL COMMENT '手机区号',
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `email` (`email`) USING BTREE,
  KEY `mobile` (`mobile`) USING BTREE,
  KEY `facebookid` (`facebookid`) USING BTREE,
  KEY `googleid` (`googleid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_0` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_1` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_10` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_11` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_12` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_13` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_14` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_15` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_16` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_17` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_18` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_19` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_2` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_20` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_21` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_22` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_23` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_24` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_25` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_26` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_27` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_28` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_29` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_3` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_30` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_31` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_4` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_5` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_6` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_7` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_8` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userinfo_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo_9` (
  `uid` int(10) unsigned NOT NULL COMMENT 'uid',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `img` text,
  `lv` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `username_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `username_8` (
  `username` char(15) NOT NULL,
  `uid` bigint(20) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uname` (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户名';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-02 13:46:45
