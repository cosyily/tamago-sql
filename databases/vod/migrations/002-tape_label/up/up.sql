START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(002,'UP','tape_label',''); COMMIT;
CREATE TABLE `tmg_tape_label` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `file_id` varchar(100) DEFAULT NULL COMMENT 'File ID of the tape.',
 `label_id` int(11) DEFAULT NULL COMMENT 'Label ID',
 `created_time` datetime DEFAULT NULL COMMENT 'Date time of when the label inserted',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='Linking tape and label with file id.';
