-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-admin-contetns-slave.tamago.db    Database: hm_admin
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_admin`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_admin` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_admin`;

--
-- Table structure for table `admin_account`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_account` (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '用户昵称',
  `uaccount` varchar(50) NOT NULL COMMENT '用户账户',
  `upwd` varchar(50) NOT NULL COMMENT '密码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `login_count` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `last_login_time` int(11) DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(20) DEFAULT NULL COMMENT '最后登录ip',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '权限组id',
  `adduid` int(11) NOT NULL COMMENT '添加人',
  `notebook` text COMMENT '备忘录',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1激活 2没激活',
  `update_time` int(11) NOT NULL COMMENT '最后修改时间',
  `user_id` int(11) DEFAULT '1870709' COMMENT '对应的前端ID',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_log_2016`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_log_2016` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(50) DEFAULT NULL COMMENT '操作用户名',
  `ip` varchar(16) DEFAULT NULL COMMENT '登陆用户ip',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '修改时间',
  `menu_sign` varchar(64) DEFAULT NULL COMMENT '操作标示',
  `do_msg` varchar(128) DEFAULT NULL COMMENT '操作信息',
  `do_type` varchar(16) DEFAULT NULL COMMENT '操作类型',
  `do_content` text COMMENT '操作内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户行为日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_log_2017`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_log_2017` (
  `id` int(11) NOT NULL COMMENT '主键',
  `user_name` varchar(50) DEFAULT NULL COMMENT '操作用户名',
  `ip` varchar(16) DEFAULT NULL COMMENT '登陆用户ip',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '修改时间',
  `menu_sign` varchar(64) DEFAULT NULL COMMENT '操作标示',
  `do_msg` varchar(128) DEFAULT NULL COMMENT '操作信息',
  `do_type` varchar(16) DEFAULT NULL COMMENT '操作类型',
  `do_content` text COMMENT '操作内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户行为日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_log_2018`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_log_2018` (
  `id` int(11) NOT NULL COMMENT '主键',
  `user_name` varchar(50) DEFAULT NULL COMMENT '操作用户名',
  `ip` varchar(16) DEFAULT NULL COMMENT '登陆用户ip',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '修改时间',
  `menu_sign` varchar(64) DEFAULT NULL COMMENT '操作标示',
  `do_msg` varchar(128) DEFAULT NULL COMMENT '操作信息',
  `do_type` varchar(16) DEFAULT NULL COMMENT '操作类型',
  `do_content` text COMMENT '操作内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户行为日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_menu`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `pid` int(11) DEFAULT '0' COMMENT '父级菜单ID',
  `type` enum('1','2') DEFAULT '1' COMMENT '分类类型，显示1，不显示2',
  `name` varchar(64) DEFAULT NULL COMMENT '菜单名称',
  `sign` varchar(64) DEFAULT NULL COMMENT '菜单标志',
  `status` enum('1','2') DEFAULT '1' COMMENT '菜单状态  1是使用，2是未使用',
  `url` varchar(255) DEFAULT NULL COMMENT '菜单url',
  `sort` int(11) DEFAULT '50' COMMENT '排序',
  `remark` text COMMENT '备注说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_power`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_power` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '权限表主键',
  `adminid` int(11) DEFAULT NULL COMMENT '用户id',
  `branchid` int(11) DEFAULT NULL COMMENT '用户部门id',
  `power_string` text COMMENT '用户权限字符串,用户逗号隔开',
  `power_gid` text COMMENT '用户数据权限设置，游戏分类',
  `opusername` varchar(50) DEFAULT NULL COMMENT '操作用户',
  `updatetime` int(11) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='后台权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `change_money_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `change_money_log` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(20) DEFAULT NULL COMMENT '用户id',
  `nickname` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `bizhong` int(1) DEFAULT NULL COMMENT '币种 1表示猫币 2表示仙豆',
  `num` decimal(9,2) DEFAULT NULL COMMENT '数量',
  `reason` varchar(255) DEFAULT NULL COMMENT '原因',
  `message` varchar(255) DEFAULT NULL COMMENT '短消息',
  `addtime` varchar(255) DEFAULT NULL COMMENT '添加时间',
  `edituser` varchar(255) DEFAULT NULL COMMENT '操作人',
  `addtype` tinyint(1) DEFAULT '1' COMMENT '添加类型',
  PRIMARY KEY (`Id`),
  KEY `uid` (`uid`),
  KEY `bizhong` (`bizhong`)
) ENGINE=InnoDB AUTO_INCREMENT=3041 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_admin_jianhuang_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_admin_jianhuang_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` bigint(20) DEFAULT NULL,
  `ts` int(11) NOT NULL COMMENT '插入时间',
  `streamname` varchar(40) DEFAULT '',
  `nickname` varchar(255) DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL COMMENT '0:色情 1:性感 2:正常',
  `pic` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `isreview` tinyint(2) DEFAULT '0' COMMENT '是否需要复核',
  `roomNumber` varchar(20) NOT NULL DEFAULT '' COMMENT '房间号',
  `rate` int(11) NOT NULL COMMENT '鉴黄分数',
  `raw_data` text,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9190 DEFAULT CHARSET=utf8 COMMENT='鉴黄记录日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_guard_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_guard_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `room_number` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `get_type` tinyint(4) DEFAULT NULL,
  `day` tinyint(4) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `user_msg` varchar(255) DEFAULT NULL,
  `anchor_msg` varchar(255) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `admin_name` varchar(64) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=291 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_memberbadgelog`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_memberbadgelog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL COMMENT '徽章导入的标题',
  `cont` varchar(255) DEFAULT NULL COMMENT '徽章导入的内容',
  `errorlog` text COMMENT '错误日志',
  `opusername` varchar(50) DEFAULT NULL,
  `optime` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1：成功 2：失败',
  `badgetype` mediumint(9) DEFAULT '0' COMMENT '徽章类型',
  `sucnum` mediumint(8) DEFAULT '0' COMMENT '成功人士',
  `failnum` mediumint(8) DEFAULT '0' COMMENT '失败人数',
  `badgename` varchar(100) DEFAULT NULL COMMENT '徽章名称',
  `owntime` mediumint(9) DEFAULT '0' COMMENT '拥有的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_mobile_active`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_mobile_active` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `activename` varchar(50) DEFAULT NULL COMMENT '活动名称',
  `typeArea` tinyint(1) DEFAULT '1' COMMENT '状态 1分类 2标签 3房间号',
  `gameVal` text COMMENT '分类',
  `labelVal` text COMMENT '标签',
  `starttime` int(11) DEFAULT NULL COMMENT '开始时间',
  `endtime` int(11) DEFAULT NULL COMMENT '结束时间',
  `roomNumbers` text COMMENT '房间号',
  `opusername` varchar(50) NOT NULL COMMENT '添加人',
  `imgUrl` varchar(255) NOT NULL COMMENT '图片url',
  `url` varchar(255) NOT NULL COMMENT '活动url',
  `weight` smallint(4) DEFAULT '0' COMMENT '权重',
  `addtime` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '最后修改时间',
  `type` varchar(50) NOT NULL COMMENT '类型 1：ios 2：ipad 3：android',
  `anchorShow` tinyint(1) DEFAULT '0' COMMENT '主播端是否显示 1：显示 0：不显示',
  `less_version` varchar(128) DEFAULT NULL COMMENT '用于移动端：小于等于这个版本号才显示',
  `more_version` varchar(128) DEFAULT NULL COMMENT '用于移动端：大于等于这个版本号才显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_plate_control`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_plate_control` (
  `hid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) DEFAULT NULL,
  `contents` text,
  `domain` varchar(55) DEFAULT NULL,
  `is_baidusearch` tinyint(4) DEFAULT '0' COMMENT '是否开启百度统计，1为开启，0为关闭',
  `is_autodown` tinyint(4) DEFAULT '0' COMMENT '是否开启自动弹出下载 1为开启，0为关闭',
  `operatename` varchar(25) DEFAULT NULL,
  `operatetime` int(11) DEFAULT NULL,
  PRIMARY KEY (`hid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_platform_auth`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_platform_auth` (
  `id` int(22) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rid` int(22) NOT NULL COMMENT 'type:1 活动id type:2 房间id',
  `aid` int(22) NOT NULL COMMENT '基础权限id',
  `type` tinyint(1) NOT NULL COMMENT 'type：1 移动端房间活动 type：2 公共广告',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_robot_msg_logs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_robot_msg_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rmid` int(10) unsigned NOT NULL COMMENT 'robot_msg的id',
  `room_number` int(10) unsigned NOT NULL,
  `adminid` int(10) unsigned NOT NULL,
  `author` varchar(20) NOT NULL,
  `action` varchar(25) NOT NULL,
  `info` text NOT NULL,
  `time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8197 DEFAULT CHARSET=utf8 COMMENT='弹幕机操作日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_robot_msgs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_robot_msgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_number` int(10) unsigned NOT NULL,
  `cid` int(10) unsigned NOT NULL,
  `gid` int(10) unsigned NOT NULL,
  `cname` varchar(30) NOT NULL,
  `start_time` int(10) unsigned NOT NULL,
  `end_time` int(10) unsigned NOT NULL,
  `msg_count` int(10) unsigned NOT NULL,
  `msg_count_min` int(10) unsigned NOT NULL,
  `author` varchar(10) NOT NULL,
  `msg1` text NOT NULL,
  `msg_status1` tinyint(3) unsigned NOT NULL COMMENT '启动状态',
  `count_sec1` tinyint(3) unsigned NOT NULL COMMENT 'n个/秒',
  `return_count1` int(10) unsigned NOT NULL COMMENT '停止后返回的弹幕数量',
  `msg2` text NOT NULL,
  `msg_status2` tinyint(3) unsigned NOT NULL COMMENT '启动状态',
  `count_sec2` tinyint(3) unsigned NOT NULL COMMENT 'n个/秒',
  `return_count2` int(10) unsigned NOT NULL COMMENT '停止后返回的弹幕数量',
  `msg3` text NOT NULL,
  `msg_status3` tinyint(3) unsigned NOT NULL COMMENT '启动状态',
  `count_sec3` tinyint(3) unsigned NOT NULL COMMENT 'n个/秒',
  `return_count3` int(10) unsigned NOT NULL COMMENT '停止后返回的弹幕数量',
  `msg4` text NOT NULL,
  `msg_status4` tinyint(3) unsigned NOT NULL COMMENT '启动状态',
  `count_sec4` tinyint(3) unsigned NOT NULL COMMENT 'n个/秒',
  `return_count4` int(10) unsigned NOT NULL COMMENT '停止后返回的弹幕数量',
  `msg5` text NOT NULL,
  `msg_status5` tinyint(3) unsigned NOT NULL COMMENT '启动状态',
  `count_sec5` tinyint(3) unsigned NOT NULL COMMENT 'n个/秒',
  `return_count5` int(10) unsigned NOT NULL COMMENT '停止后返回的弹幕数量',
  `create_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gid` (`gid`,`author`),
  KEY `room_number` (`room_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='弹幕机器人';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_send_gift_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_send_gift_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` text COMMENT '备注',
  `gift` varchar(64) DEFAULT NULL,
  `uid_count` int(11) DEFAULT NULL COMMENT '总人数',
  `uid_succeed` int(11) DEFAULT NULL COMMENT '成功人数',
  `uid_fail` int(11) DEFAULT NULL COMMENT '失败人数',
  `addtime` int(11) DEFAULT NULL COMMENT '发送时间',
  `adminid` int(11) DEFAULT NULL COMMENT '发送者',
  `result` text COMMENT '结果',
  `adminname` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_special`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_special` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '专题表主键',
  `gid` int(11) DEFAULT NULL COMMENT '游戏分类id',
  `gname` varchar(32) DEFAULT NULL COMMENT '游戏分类名',
  `name` varchar(50) DEFAULT NULL COMMENT '赛事全称',
  `short_name` varchar(50) DEFAULT NULL COMMENT '赛事简称',
  `title` varchar(255) DEFAULT NULL,
  `desc` text,
  `key` varchar(255) DEFAULT NULL,
  `special_type` varchar(255) DEFAULT NULL COMMENT '专题页功能分类',
  `start_time` int(11) DEFAULT NULL COMMENT '开始时间',
  `end_time` int(11) DEFAULT NULL COMMENT '结束时间',
  `addtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '最后修改时间',
  `opusername` varchar(50) DEFAULT NULL COMMENT '上一次操作的用户',
  `is_top` tinyint(4) DEFAULT '2' COMMENT '是否置顶  1是，2否',
  `domain` varchar(128) DEFAULT NULL COMMENT '专题域名',
  `status` tinyint(1) DEFAULT '2' COMMENT '专题状态，1是已发布，2未发布，默认2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='专题表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_special_channel`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_special_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '绑定房间表主键',
  `sid` int(11) DEFAULT NULL COMMENT '主题id',
  `room_number` int(11) DEFAULT NULL COMMENT '绑定的房间号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `room_number` (`room_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_special_content`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_special_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '专题内容表主键',
  `sid` int(11) DEFAULT NULL COMMENT '专题id',
  `special_field` varchar(64) DEFAULT NULL COMMENT '专题功能字段',
  `special_content` text COMMENT '专题内容',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态 1使用 2不使用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_userblackmenu`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_userblackmenu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` bigint(20) DEFAULT NULL,
  `closestarttime` int(11) DEFAULT NULL COMMENT '开始封禁的时刻',
  `closeday` varchar(100) DEFAULT NULL COMMENT '封禁天数；alway：表示永久',
  `closereason` varchar(500) DEFAULT NULL COMMENT '封禁原因',
  `opusername` varchar(100) DEFAULT NULL COMMENT '最后操作人',
  `updatetime` int(11) DEFAULT NULL COMMENT '最近操作时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uidkey` (`uid`) USING BTREE COMMENT 'uid唯一索引'
) ENGINE=InnoDB AUTO_INCREMENT=19183 DEFAULT CHARSET=utf8 COMMENT='用户黑名单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_white_list`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_white_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `use_name` varchar(20) NOT NULL COMMENT '使用者',
  `nickname` varchar(20) NOT NULL COMMENT '昵称',
  `note` varchar(255) NOT NULL COMMENT '备注',
  `create_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='白名单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hot_search_words`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hot_search_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '热词名称',
  `type` tinyint(2) DEFAULT NULL COMMENT '1：android 2：ios',
  `order` int(11) DEFAULT NULL COMMENT '权重',
  `starttime` int(11) DEFAULT NULL,
  `endtime` int(11) DEFAULT NULL,
  `is_new` tinyint(2) DEFAULT NULL COMMENT '是否是new',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态1：开启0：关闭',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `opusername` varchar(100) DEFAULT NULL COMMENT '添加人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `robot_danmu_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `robot_danmu_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(50) DEFAULT NULL COMMENT '操作用户名',
  `ip` varchar(16) DEFAULT NULL COMMENT '登陆用户ip',
  `logintime` datetime DEFAULT NULL COMMENT '登陆时间',
  PRIMARY KEY (`id`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='弹幕机器人登陆日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `share_content`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `share_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '分享标题',
  `type` tinyint(2) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `order` varchar(255) DEFAULT NULL,
  `starttime` int(11) DEFAULT NULL,
  `endtime` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `opusername` varchar(100) DEFAULT NULL COMMENT '添加人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_universal_password`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_universal_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto increment id',
  `admin_id` int(11) DEFAULT NULL COMMENT 'Admin ID',
  `admin_name` varchar(255) DEFAULT NULL COMMENT 'Admin Name',
  `user_id` int(11) DEFAULT NULL COMMENT 'User ID',
  `reason` varchar(255) DEFAULT NULL COMMENT 'Reason',
  `random_code` varchar(50) DEFAULT NULL COMMENT 'Random Code integer only',
  `master_password` varchar(50) DEFAULT NULL COMMENT 'Master Password',
  `created_time` datetime DEFAULT NULL COMMENT 'Created time',
  `login_time` datetime DEFAULT NULL COMMENT 'Login Time',
  PRIMARY KEY (`id`),
  KEY `idx_random_code_login_time_created_time` (`random_code`,`login_time`,`created_time`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-01 16:08:26
