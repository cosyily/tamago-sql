START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(003,'UP','chat_monitor',''); COMMIT;
CREATE TABLE `tmg_chat_monitor` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `message` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `color_barrage` varchar(255) DEFAULT NULL,
  `guard_barrage` varchar(255) DEFAULT NULL,
  `admin_private_chat` varchar(255) DEFAULT NULL,
  `hot_word` varchar(255) DEFAULT NULL,
  `alert_flag` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
