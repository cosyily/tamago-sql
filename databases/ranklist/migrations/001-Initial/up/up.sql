START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(001,'UP','Initial',''); COMMIT;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: tamago-v2-prod-other-raffle-ranklist.cdcumqgjhhxx.ap-southeast-1.rds.amazonaws.com    Database: hm_ranklist
-- ------------------------------------------------------
-- Server version	5.6.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_ranklist`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_ranklist` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_ranklist`;

--
-- Table structure for table `hm_anchor_popular`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_popular` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `daytime` char(12) DEFAULT NULL,
  `freebean` int(11) DEFAULT '0' COMMENT '当天收到的仙豆值',
  `is_entertainment` enum('yes','no') DEFAULT 'no' COMMENT '是否是娱乐主播',
  `unionid` int(11) DEFAULT '0' COMMENT '工会id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_day_index` (`uid`,`daytime`) USING BTREE COMMENT '一个主播一天只有一条记录',
  KEY `unionid_index` (`unionid`) USING BTREE COMMENT '工会id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主播人气周榜和日榜';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_popular_all`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_popular_all` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `freebean` int(11) DEFAULT '0' COMMENT '主播收到的总的仙豆',
  `is_entertainment` enum('yes','no') DEFAULT 'no',
  `unionid` int(11) DEFAULT '0' COMMENT '工会id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_index` (`uid`) USING BTREE COMMENT '一个主播只有一条记录',
  KEY `unionid_index` (`unionid`) USING BTREE COMMENT '工会id索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主播人气总榜';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_streamstate`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_streamstate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主播时长表',
  `uid` int(11) DEFAULT '0' COMMENT '主播uid',
  `daytime` char(10) DEFAULT NULL COMMENT '时间',
  `play_duration` mediumint(5) DEFAULT NULL COMMENT '直播时长的秒数',
  `stream_state` enum('down','live') DEFAULT 'down' COMMENT '当前直播状态',
  `is_entertainment` enum('yes','no') DEFAULT 'no',
  `unionid` int(11) DEFAULT '0' COMMENT '主播所属工会id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uiddaytime_index` (`uid`,`daytime`) USING BTREE,
  KEY `unionid_index` (`unionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_streamstate_all`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_streamstate_all` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主播时长表',
  `uid` int(11) DEFAULT '0' COMMENT '主播uid',
  `play_duration` mediumint(5) DEFAULT NULL COMMENT '直播时长的秒数',
  `is_entertainment` enum('yes','no') DEFAULT 'no',
  `unionid` int(11) DEFAULT '0' COMMENT '主播所属工会uid',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uiddaytime_index` (`uid`) USING BTREE,
  KEY `unionid_index` (`unionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_audience_sendgift`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_audience_sendgift` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audience_uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `catcoin` decimal(13,3) DEFAULT NULL COMMENT '观众送出的猫币',
  `daytime` char(10) DEFAULT '0' COMMENT '送礼时间',
  `is_entertainment` enum('yes','no') DEFAULT 'no' COMMENT '是否是娱乐主播',
  `unionid` int(11) DEFAULT '0' COMMENT '主播所属工会uid',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auanday_index` (`audience_uid`,`anchor_uid`,`daytime`) USING BTREE COMMENT '某一个观众在某一天送给某一个主播的值是唯一的',
  KEY `unionid_index` (`unionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='富豪实力榜和粉丝贡献榜：日榜和周榜，可以写定时脚本进行定时清洗';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_audience_sendgift__halfhour`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_audience_sendgift__halfhour` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audience_uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `catcoin` decimal(13,3) DEFAULT NULL COMMENT '观众送出的猫币',
  `daytime` datetime DEFAULT NULL COMMENT '送礼时间',
  `is_entertainment` enum('yes','no') DEFAULT 'no' COMMENT '是否是娱乐主播',
  `unionid` int(11) DEFAULT '0' COMMENT '主播所属工会uid',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auanday_index` (`audience_uid`,`anchor_uid`,`daytime`) USING BTREE COMMENT '某一个观众在某一天送给某一个主播的值是唯一的',
  KEY `unionid_index` (`unionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='富豪实力榜和粉丝贡献榜：日榜和周榜，可以写定时脚本进行定时清洗';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_audience_sendgift_all`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_audience_sendgift_all` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audience_uid` int(11) DEFAULT NULL COMMENT '观众uid',
  `anchor_uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `catcoin` decimal(13,3) DEFAULT NULL COMMENT '观众送出的猫币',
  `is_entertainment` enum('yes','no') DEFAULT 'no' COMMENT '是否是娱乐主播',
  `unionid` int(11) DEFAULT '0' COMMENT '主播所属工会uid',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auanday_index` (`audience_uid`,`anchor_uid`) USING BTREE COMMENT '某一个观众在某一天送给某一个主播的值是唯一的',
  KEY `unionid_index` (`unionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='富豪实力榜总榜和粉丝贡献榜总榜';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_audience_sendmsg_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_audience_sendmsg_2017_10` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audience_uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `cid` int(11) DEFAULT NULL COMMENT '房间ID',
  `gid` int(11) DEFAULT NULL COMMENT '游戏分类ID',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `addtime` int(11) DEFAULT NULL COMMENT '发言时间',
  `color_type` int(11) DEFAULT NULL COMMENT '弹幕类型',
  `user_type` int(11) DEFAULT NULL COMMENT '是不是白名单用户',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `gid` (`gid`),
  KEY `addtime` (`addtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='彩色弹幕记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_audience_sendmsg_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_audience_sendmsg_2017_11` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audience_uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `cid` int(11) DEFAULT NULL COMMENT '房间ID',
  `gid` int(11) DEFAULT NULL COMMENT '游戏分类ID',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `addtime` int(11) DEFAULT NULL COMMENT '发言时间',
  `color_type` int(11) DEFAULT NULL COMMENT '弹幕类型',
  `user_type` int(11) DEFAULT NULL COMMENT '是不是白名单用户',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `gid` (`gid`),
  KEY `addtime` (`addtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='彩色弹幕记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_audience_sendmsg_2017_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_audience_sendmsg_2017_5` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audience_uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `cid` int(11) DEFAULT NULL COMMENT '房间ID',
  `gid` int(11) DEFAULT NULL COMMENT '游戏分类ID',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `addtime` int(11) DEFAULT NULL COMMENT '发言时间',
  `color_type` int(11) DEFAULT NULL COMMENT '弹幕类型',
  `user_type` int(11) DEFAULT NULL COMMENT '是不是白名单用户',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `gid` (`gid`),
  KEY `addtime` (`addtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='彩色弹幕记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_audience_sendmsg_2017_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_audience_sendmsg_2017_6` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audience_uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `cid` int(11) DEFAULT NULL COMMENT '房间ID',
  `gid` int(11) DEFAULT NULL COMMENT '游戏分类ID',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `addtime` int(11) DEFAULT NULL COMMENT '发言时间',
  `color_type` int(11) DEFAULT NULL COMMENT '弹幕类型',
  `user_type` int(11) DEFAULT NULL COMMENT '是不是白名单用户',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `gid` (`gid`),
  KEY `addtime` (`addtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='彩色弹幕记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_audience_sendmsg_2017_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_audience_sendmsg_2017_7` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audience_uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `cid` int(11) DEFAULT NULL COMMENT '房间ID',
  `gid` int(11) DEFAULT NULL COMMENT '游戏分类ID',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `addtime` int(11) DEFAULT NULL COMMENT '发言时间',
  `color_type` int(11) DEFAULT NULL COMMENT '弹幕类型',
  `user_type` int(11) DEFAULT NULL COMMENT '是不是白名单用户',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `gid` (`gid`),
  KEY `addtime` (`addtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='彩色弹幕记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_audience_sendmsg_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_audience_sendmsg_2017_8` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audience_uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `cid` int(11) DEFAULT NULL COMMENT '房间ID',
  `gid` int(11) DEFAULT NULL COMMENT '游戏分类ID',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `addtime` int(11) DEFAULT NULL COMMENT '发言时间',
  `color_type` int(11) DEFAULT NULL COMMENT '弹幕类型',
  `user_type` int(11) DEFAULT NULL COMMENT '是不是白名单用户',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `gid` (`gid`),
  KEY `addtime` (`addtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='彩色弹幕记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_audience_sendmsg_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_audience_sendmsg_2017_9` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `audience_uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `cid` int(11) DEFAULT NULL COMMENT '房间ID',
  `gid` int(11) DEFAULT NULL COMMENT '游戏分类ID',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `addtime` int(11) DEFAULT NULL COMMENT '发言时间',
  `color_type` int(11) DEFAULT NULL COMMENT '弹幕类型',
  `user_type` int(11) DEFAULT NULL COMMENT '是不是白名单用户',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `gid` (`gid`),
  KEY `addtime` (`addtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='彩色弹幕记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_rankattach`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_rankattach` (
  `cid` int(11) NOT NULL,
  `is_entertainment` enum('yes','no') DEFAULT 'no' COMMENT '是否是娱乐主播',
  `uid` int(11) DEFAULT NULL COMMENT '主播uid',
  `unionid` int(11) DEFAULT NULL COMMENT '工会id',
  `is_jingcai` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0房间无竞猜|1房间有竞猜',
  PRIMARY KEY (`cid`),
  UNIQUE KEY `uid_index` (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_guard_expire`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_guard_expire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `expire` decimal(10,2) DEFAULT NULL COMMENT '守护值',
  `is_gift` tinyint(4) DEFAULT '0' COMMENT '0 购买守护  其它 礼物ID',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `anchor_id` (`anchor_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=459 DEFAULT CHARSET=utf8 COMMENT='守护值';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_wangyi`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_wangyi` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_id` varchar(100) NOT NULL COMMENT '订单号',
  `status` tinyint(1) NOT NULL COMMENT '订单状态0：下单 1：支付成功 2：已发货 3：交易成功 4：交易失败 5：取消订单',
  `pay_amount` int(10) NOT NULL COMMENT '支付金额 是实际金额100倍',
  `commission` int(10) NOT NULL COMMENT '预估佣金 是实际金额100倍',
  `app_type` tinyint(1) NOT NULL COMMENT '购买终端 0 = web 1=wap',
  `addtime` int(10) NOT NULL COMMENT '添加时间',
  `updatetime` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `status` (`status`),
  KEY `pay_amount` (`pay_amount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网易嘉年华订单';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-02 11:51:18
