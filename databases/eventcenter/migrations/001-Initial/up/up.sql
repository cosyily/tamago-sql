START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(001,'UP','Initial',''); COMMIT;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-error.tamago.db    Database: hm_eventcenter
-- ------------------------------------------------------
-- Server version	5.6.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_eventcenter`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_eventcenter` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_eventcenter`;

--
-- Table structure for table `hm_event_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_event_info` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `event_id` int(12) NOT NULL COMMENT '所属赛事ID',
  `home_team` varchar(50) NOT NULL DEFAULT '' COMMENT '主队名称',
  `visiting_team` varchar(50) NOT NULL DEFAULT '' COMMENT '客队名称',
  `channel_id` int(12) NOT NULL COMMENT '频道',
  `match_time` int(10) NOT NULL DEFAULT '0',
  `end_time` int(10) NOT NULL COMMENT '比赛结束时间',
  `match_date` int(10) NOT NULL DEFAULT '0',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `addtime` int(10) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `updatetime` int(10) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `note` varchar(100) NOT NULL COMMENT '备注',
  `home_result` tinyint(4) DEFAULT NULL,
  `visiting_result` tinyint(4) DEFAULT NULL,
  `home_rate` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '主场赔率,系统每20秒更新一次',
  `visiting_rate` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '客场赔率,系统每20秒更新一次',
  `victory_team` varchar(50) NOT NULL DEFAULT '' COMMENT '胜利的队伍',
  `is_open` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否开启,默认不开启',
  `open_time` int(10) NOT NULL DEFAULT '0' COMMENT '最后操作时间',
  `is_settle` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否结算',
  `settle_time` int(10) NOT NULL DEFAULT '0' COMMENT '结算时间',
  `is_assign` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否分发仙豆,0未分发,1已分发,2回滚',
  `assign_time` int(10) NOT NULL DEFAULT '0' COMMENT '分发操作最后更新时间',
  `is_top` tinyint(2) DEFAULT '0' COMMENT '是否推流',
  `yugao_id` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '主播uid',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '去向链接',
  `home_team_id` int(11) NOT NULL COMMENT '主队的ID',
  `visiting_team_id` int(11) NOT NULL COMMENT '客队的ID',
  `game_id` int(11) DEFAULT '0' COMMENT '所属游戏类型ID，默认为当前赛事所属游戏类型ID：表示当前赛事ID',
  `last_operator` varchar(10) DEFAULT NULL COMMENT '最后操作人',
  `istop` tinyint(1) DEFAULT NULL COMMENT '是否置顶',
  `score` varchar(10) NOT NULL COMMENT '比分',
  `is_hot` tinyint(1) DEFAULT '0' COMMENT '是否是热门比赛；0:表示不热门，1：表示热门',
  `watching_focus` varchar(255) DEFAULT '' COMMENT '看点',
  `video_record_url` varchar(255) DEFAULT '' COMMENT '录像地址',
  `redirect_urlinfo` text COMMENT '去向地址信息，存储为map类型;格式为{offical:[XXX,XXX ],personal1:[XXX,"url":XXX],personal2:[XXX,"url":XXX] }',
  `match_end_date` int(11) NOT NULL COMMENT '比赛结束日期',
  `match_session` char(50) NOT NULL COMMENT '比赛场次',
  `is_sync_guess` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否同步至竞猜：0：表示不同步；1：表示同步',
  PRIMARY KEY (`id`),
  KEY `yugao_id` (`yugao_id`),
  KEY `event_id` (`event_id`),
  KEY `match_date` (`match_date`,`is_del`),
  KEY `match_end_time_index` (`match_end_date`),
  KEY `channelid_index` (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='赛事信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_event_list`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_event_list` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL COMMENT '赛事名称',
  `short_name` varchar(50) NOT NULL DEFAULT '' COMMENT '简称',
  `start_time` int(10) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(10) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `event_img` varchar(120) NOT NULL COMMENT '赛事图片',
  `images_2` varchar(100) NOT NULL,
  `sort` int(2) NOT NULL COMMENT '排序值',
  `is_del` tinyint(1) NOT NULL COMMENT '是否删除',
  `addtime` int(10) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `updatetime` int(10) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `note` varchar(100) NOT NULL COMMENT '备注',
  `default_cid` int(11) NOT NULL DEFAULT '0' COMMENT '默认频道',
  `url_rule` varchar(20) NOT NULL DEFAULT '' COMMENT '伪静态规则',
  `template_name` varchar(20) NOT NULL DEFAULT '' COMMENT '模板名',
  `is_top` tinyint(4) NOT NULL DEFAULT '0',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '所属游戏类型',
  `event_jd` varchar(50) DEFAULT NULL COMMENT '赛事进度',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态:1：表示启用；0：表示禁用',
  `sponsor` varchar(20) DEFAULT NULL COMMENT '主办方',
  `match_logo` varchar(120) DEFAULT NULL COMMENT '赛事logo',
  `match_day` int(11) DEFAULT NULL COMMENT '比赛天数',
  `match_mode` varchar(20) DEFAULT NULL COMMENT '比赛模式',
  `match_place` varchar(20) DEFAULT NULL COMMENT '比赛地点',
  `match_bonus` varchar(20) DEFAULT NULL COMMENT '比赛奖金',
  `match_rating` tinyint(2) DEFAULT NULL COMMENT '比赛评级',
  `last_operator` varchar(10) DEFAULT NULL COMMENT '最后操作人',
  `event_description` text COMMENT '赛事描述',
  `event_area` mediumint(8) DEFAULT '0' COMMENT '赛事地区',
  `redirect_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `is_del` (`is_del`),
  KEY `start_time` (`start_time`),
  KEY `end_time` (`end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='比赛赛事表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_event_team`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_event_team` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `teamname` varchar(100) DEFAULT NULL COMMENT '队伍名称',
  `logo` varchar(250) DEFAULT NULL,
  `description` text,
  `createtime` int(11) DEFAULT NULL,
  `updatetime` int(11) DEFAULT NULL,
  `team_zone` varchar(10) DEFAULT NULL COMMENT '所属分区',
  `is_top` tinyint(1) DEFAULT NULL COMMENT '是否置顶',
  `gid` int(11) DEFAULT NULL COMMENT '游戏分类',
  `team_members` varchar(120) DEFAULT NULL COMMENT '战队成员',
  `other_members` varchar(120) DEFAULT NULL COMMENT '其他成员',
  `last_operator` varchar(10) DEFAULT NULL COMMENT '最后操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_eventbrand`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_eventbrand` (
  `ebid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '赛事品牌id',
  `brand_name` varchar(200) DEFAULT NULL COMMENT '品牌名称',
  `brand_pic` varchar(200) DEFAULT NULL COMMENT '品牌图片',
  `brand_desc` varchar(200) DEFAULT NULL COMMENT '品牌简介',
  `event_type` text COMMENT '拥有的赛事类型',
  `event_series` text COMMENT '赛事系列',
  `event_place` text COMMENT '地点',
  `is_top` tinyint(1) DEFAULT '0' COMMENT '是否置顶；默认0：否；1：表示是',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除；默认为0：表示正常；1：表示删除',
  `createtime` int(11) DEFAULT NULL,
  `updatetime` int(11) DEFAULT NULL,
  `opusername` varchar(100) DEFAULT NULL COMMENT '最后操作人',
  `sort` int(11) DEFAULT '0' COMMENT '权重',
  PRIMARY KEY (`ebid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_recommendevent`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_recommendevent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(11) DEFAULT NULL COMMENT '推荐内容ID',
  `recommendcode` varchar(100) DEFAULT NULL COMMENT '推荐位关系码',
  `remark` varchar(300) DEFAULT NULL COMMENT '备注信息',
  `opusername` varchar(50) DEFAULT NULL COMMENT '上一次操作的用户',
  `order` mediumint(9) DEFAULT '0' COMMENT '推荐的顺序，越大排前面',
  `linkurl` varchar(128) DEFAULT NULL COMMENT '推荐链接',
  `images` varchar(255) DEFAULT NULL COMMENT '推荐图片',
  `ext` varchar(255) DEFAULT NULL COMMENT '扩展字段',
  `addtime` int(11) DEFAULT '0' COMMENT '添加时间',
  `updatetime` int(11) DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-01 16:11:18
