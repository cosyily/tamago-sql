-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-vod-replica.tamago.db    Database: hm_vod
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_vod`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_vod` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_vod`;

--
-- Table structure for table `hm_tape`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_tape` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(13) NOT NULL DEFAULT '0' COMMENT 'channel id',
  `room_num` int(10) unsigned NOT NULL DEFAULT '0',
  `uid` int(13) NOT NULL DEFAULT '0' COMMENT '主播uid',
  `steam` varchar(50) NOT NULL DEFAULT '' COMMENT 'steam id',
  `gid` int(13) NOT NULL DEFAULT '0' COMMENT '分类',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始录制',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束录制',
  `duration` int(11) NOT NULL DEFAULT '0' COMMENT '时长',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '主播昵称',
  `file_id` varchar(256) NOT NULL DEFAULT '' COMMENT 'cdn文件ID',
  `origin_url` varchar(256) NOT NULL DEFAULT '' COMMENT '原始url',
  `status` enum('wait','trans','done','fail') NOT NULL DEFAULT 'wait' COMMENT '转码状态',
  `vid` int(11) DEFAULT NULL COMMENT '视频表id',
  `create_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `room_num` (`room_num`),
  KEY `file_id` (`file_id`(255)),
  KEY `gid` (`gid`),
  KEY `start_time` (`start_time`)
) ENGINE=InnoDB AUTO_INCREMENT=255731 DEFAULT CHARSET=utf8 COMMENT='录制信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `intro` text COMMENT '简介',
  `cover` varchar(256) DEFAULT '' COMMENT '封面',
  `screenshots` text COMMENT '截图',
  `duration` int(11) NOT NULL DEFAULT '0' COMMENT '时长',
  `cid` int(13) NOT NULL DEFAULT '0' COMMENT 'channel id',
  `gid` int(13) NOT NULL DEFAULT '0' COMMENT '分类',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '类型id',
  `origin` int(11) NOT NULL DEFAULT '1' COMMENT '来源',
  `publish` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '是否发布',
  `uid` int(13) NOT NULL DEFAULT '0' COMMENT '上传者id或主播ID',
  `username` varchar(50) NOT NULL DEFAULT '',
  `room_num` int(10) unsigned NOT NULL DEFAULT '0',
  `file_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'cdn文件id',
  `rate` decimal(5,2) NOT NULL DEFAULT '3.00' COMMENT '播放量调整比例',
  `views` int(11) NOT NULL DEFAULT '0' COMMENT '显示播放量',
  `real_views` int(11) NOT NULL DEFAULT '0' COMMENT 'cdn播放量',
  `vaddress` text NOT NULL COMMENT 'json',
  `opusername` varchar(50) DEFAULT '',
  `upload_time` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `barrage_number` int(11) NOT NULL DEFAULT '0' COMMENT '弹幕条数',
  PRIMARY KEY (`id`),
  KEY `gid` (`gid`),
  KEY `uid` (`uid`),
  KEY `cid` (`cid`),
  KEY `file_id` (`file_id`),
  KEY `views` (`views`),
  KEY `type` (`type`),
  KEY `upload_time` (`upload_time`)
) ENGINE=InnoDB AUTO_INCREMENT=2312 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0000`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0000` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0001`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0001` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0002`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0002` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0003`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0003` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0004`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0004` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0005`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0005` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0006`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0006` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0007`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0007` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0008`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0008` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0009`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0009` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0010`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0010` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0011`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0011` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0012`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0012` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0013`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0013` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0014`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0014` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0015`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0015` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0016`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0016` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0017`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0017` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0018`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0018` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0019`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0019` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0020`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0020` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0021`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0021` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0022`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0022` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0023`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0023` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0024`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0024` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0025`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0025` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0026`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0026` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0027`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0027` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0028`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0028` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0029`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0029` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0030`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0030` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0031`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0031` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0032`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0032` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0033`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0033` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0034`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0034` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0035`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0035` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0036`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0036` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0037`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0037` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0038`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0038` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0039`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0039` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0040`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0040` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0041`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0041` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0042`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0042` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0043`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0043` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0044`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0044` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0045`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0045` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0046`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0046` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0047`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0047` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0048`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0048` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_video_barrage_0049`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_video_barrage_0049` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) NOT NULL DEFAULT '0',
  `content` varchar(100) NOT NULL DEFAULT '',
  `color` varchar(7) DEFAULT '',
  `uid` varchar(100) NOT NULL DEFAULT '',
  `rtime` int(11) NOT NULL DEFAULT '0' COMMENT '相对时间',
  `rtime_micro` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `vid` (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-02 13:47:52
