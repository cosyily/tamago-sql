START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(009,'UP','daily_checkin_tables',''); COMMIT;

# Dump of table tmg_user_daily_checkin
# ------------------------------------------------------------

CREATE TABLE `tmg_user_daily_checkin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `checkin_ts` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tmg_user_daily_checkin_uid_checkin_ts` (`uid`,`checkin_ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tmg_user_daily_day
# ------------------------------------------------------------

CREATE TABLE `tmg_user_daily_day` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `day` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tmg_user_daily_reward` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tmg_user_daily_rewards
# ------------------------------------------------------------

CREATE TABLE `tmg_user_daily_rewards` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) unsigned NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `gift_id` int(10) unsigned NOT NULL DEFAULT '0',
  `qty` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
