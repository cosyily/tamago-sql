#!/bin/sh
usage ()
{
  echo 'Usage : '$0' <migration_description>'
  exit
}

if [ -z "$1" ]
  then
    usage
fi

#get config
. ./database.config

echo 'Checking if tables exists in dev...'
mysql -h $dev_host -u$dev_user -p$dev_pass -e "use $dev_database; \. ../../src/scripts/create_tables_if_not_exists.sql"
if [ ! $? == 0 ]; then
    echo 'cannot check if tables exists in dev'
    exit 1
fi

echo 'Checking if tables exists in stg...'
mysql -h $stg_host -u$stg_user -p$stg_pass -e "use $stg_database; \. ../../src/scripts/create_tables_if_not_exists.sql"
if [ ! $? == 0 ]; then
    echo 'cannot check if tables exists in stg'
    exit 1
fi

#acquire migration lock
#check if migration has anything.. if not then do nothing
#if migration has something, then we'll need to deploy to latest version


MIGRATION_DESC=$1
VER_IN_REPO=`ls ./migrations| sort -r | head -1 | cut -c1-3`
if [ -z "$VER_IN_REPO" ]
  then
   VER_IN_REPO=0
fi
NEXT_VERSION=`expr $VER_IN_REPO + 1`
NEXT_VERSION=`printf "%03d" $NEXT_VERSION`
MIGRATION_NAME=$NEXT_VERSION-$MIGRATION_DESC

#create migration
mkdir ./migrations/$MIGRATION_NAME
cd ./migrations/$MIGRATION_NAME
MIGRATION_DIRECTORY=`pwd`
echo 'Directory '$MIGRATION_DIRECTORY' has been created...'

mkdir $MIGRATION_DIRECTORY/up
mkdir $MIGRATION_DIRECTORY/down

echo 'START TRANSACTION; insert into database_version(version,migration_type,description,reason) values('$NEXT_VERSION','\''UP'\'','\'$MIGRATION_DESC\'','\'\''); COMMIT;' >> $MIGRATION_DIRECTORY/up/up.sql
echo 'START TRANSACTION; insert into database_version(version,migration_type,description,reason) values('$NEXT_VERSION','\''DOWN'\'','\'$MIGRATION_DESC\'','\'\''); COMMIT;' >> $MIGRATION_DIRECTORY/down/down.sql

echo 'Generating up and down migration...'

#generates diff here...




