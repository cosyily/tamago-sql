START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(002,'DOWN','channel_label_tab_category',''); COMMIT;
ALTER TABLE `hm_contents`.`hm_channel_label`
  DROP COLUMN tab_display,
  DROP COLUMN category_display;
