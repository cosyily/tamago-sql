START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(001,'UP','Initial',''); COMMIT;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-error.tamago.db    Database: hm_error_log
-- ------------------------------------------------------
-- Server version	5.6.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_error_log`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_error_log` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_error_log`;

--
-- Table structure for table `error_log_2016_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2016_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2016_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2016_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2016_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2016_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2016_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2016_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` varchar(200) NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2016_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2016_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2016_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2016_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2016_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2016_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` varchar(200) NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2016_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2016_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144217 DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5935 DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2017_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2018_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2018_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13680 DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2018_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2018_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15055 DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2018_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2018_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20638 DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2018_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2018_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19237 DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2018_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2018_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46674 DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2018_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2018_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32650 DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2018_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2018_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38214 DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `error_log_2018_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_log_2018_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text NOT NULL COMMENT '参数',
  `uid` bigint(20) NOT NULL,
  `error_no` int(11) NOT NULL COMMENT '错误号',
  `ts` int(11) NOT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=443 DEFAULT CHARSET=utf8 COMMENT='错误日志库';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-01 16:11:00
