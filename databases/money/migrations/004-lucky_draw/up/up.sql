START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(004,'UP','lucky_draw',''); COMMIT;
CREATE TABLE `tmg_luckydraw_item_won` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `luckydraw_id` int(10) unsigned NOT NULL,
  `uid` int(11) unsigned NOT NULL,
  `item_index` tinyint(3) unsigned NOT NULL,
  `item_name` varchar(65) NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `createts` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `luckydraw_item_won_uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
