START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(003,'UP','money_pay_vip_type_area_code',''); COMMIT;
ALTER TABLE `hm_money`.`money_pay_2018_8` ADD COLUMN vip_type tinyint(1) NOT NULL DEFAULT '0', ADD COLUMN area_code varchar(8) NULL, ADD INDEX index_area_code (area_code), ADD INDEX index_vip_type (vip_type);
ALTER TABLE `hm_money`.`money_pay_2018_9` ADD COLUMN vip_type tinyint(1) NOT NULL DEFAULT '0', ADD COLUMN area_code varchar(8) NULL, ADD INDEX index_area_code (area_code), ADD INDEX index_vip_type (vip_type);
