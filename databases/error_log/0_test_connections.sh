#test connection for databases
#get config
. ./database.config

mysql -h $dev_host -u$dev_user -p$dev_pass -e"use $dev_database"
DEV_OK=$?
if [ $DEV_OK == 0 ]; then
    echo 'connected to dev db'
else
    echo 'cannot connect to dev db'
    exit 1
fi

mysql -h $stg_host -u$stg_user -p$stg_pass -e"exit"
STG_OK=$?
if [ $STG_OK == 0 ]; then
    echo 'connected to stg db'
else
    echo 'cannot connect to stg db'
    exit 1
fi

if [ $STG_OK == 0 ] && [ $DEV_OK == 0 ]; then
    echo 'connection tested successfully'
fi
