-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-freebean.tamago.db    Database: hm_free_bean
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_free_bean`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_free_bean` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_free_bean`;

--
-- Table structure for table `free_bean_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_0` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_1` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_10` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_11` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_12` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_13` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_14` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_15` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_16` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_17` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_18` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_19` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_2` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_20` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_21` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_22` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_23` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_24` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_25` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_26` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_27` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_28` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_29` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_3` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_30` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_31` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_4` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_5` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_6` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_7` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_8` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_bean_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_bean_9` (
  `uid` bigint(20) NOT NULL,
  `bean` decimal(9,2) unsigned NOT NULL COMMENT '仙豆',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2016_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2016_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2016_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2016_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2016_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2016_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2016_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2016_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2016_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2016_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 2竞猜-坐庄 3竞猜-下注 4爱心',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2016_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2016_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2016_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2016_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB AUTO_INCREMENT=1641504 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB AUTO_INCREMENT=65601 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2017_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2018_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2018_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB AUTO_INCREMENT=153237 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2018_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2018_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB AUTO_INCREMENT=217283 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2018_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2018_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB AUTO_INCREMENT=244178 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2018_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2018_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB AUTO_INCREMENT=208680 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2018_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2018_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB AUTO_INCREMENT=215075 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2018_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2018_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB AUTO_INCREMENT=206436 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2018_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2018_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB AUTO_INCREMENT=210408 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traderecord_2018_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traderecord_2018_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `give_uid` int(11) NOT NULL DEFAULT '0' COMMENT '送出方的uid',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '暂时废弃：记录类型 0 出 1入 (2,3是竞猜类型，4是爱心)',
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT '送出数量',
  `accept_uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收方的uid',
  `channelid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`give_uid`),
  KEY `stream` (`channelid`)
) ENGINE=InnoDB AUTO_INCREMENT=5817 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-01 16:13:22
