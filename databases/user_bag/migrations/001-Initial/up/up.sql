START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(001,'UP','Initial',''); COMMIT;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-freebean.tamago.db    Database: hm_user_bag
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_user_bag`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_user_bag` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_user_bag`;

--
-- Table structure for table `user_bag_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=23462 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=20566 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21550 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=24130 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21379 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_13` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=23953 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_14` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21750 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=22566 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_16` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21331 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_17` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21380 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_18` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21158 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_19` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=20902 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=20601 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_20` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=23514 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_21` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=18926 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_22` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21060 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_23` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=23306 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_24` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21114 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_25` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=20610 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_26` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=19631 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_27` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=20349 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_28` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=23297 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_29` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=19908 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=20140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_30` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=20745 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_31` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=22797 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21862 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=22532 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=20478 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21087 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=19565 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `get_way` varchar(64) DEFAULT '1' COMMENT '1后台发送',
  `bag_id` int(11) DEFAULT NULL COMMENT '背包当中物品id',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '过期时间',
  `num` int(11) DEFAULT NULL COMMENT '剩余礼物个数',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为礼物,2为弹幕卡,3...',
  `total` int(11) DEFAULT NULL COMMENT '总数量',
  PRIMARY KEY (`id`),
  KEY `get_bag` (`uid`,`expire_time`,`num`,`bag_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=20548 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2017_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2017_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2017_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2017_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2017_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2017_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `c_id` int(11) DEFAULT NULL,
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2017_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2017_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2017_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2017_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2017_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2017_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2017_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2017_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2018_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2018_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2018_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2018_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2018_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2018_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2018_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2018_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2048 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2018_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2018_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=1593 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2018_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2018_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=1372 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2018_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2018_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=16915 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bag_use_record_2018_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bag_use_record_2018_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `bag_id` int(11) DEFAULT NULL COMMENT '道具id',
  `user_bag_id` int(11) DEFAULT NULL COMMENT '礼物添加id',
  `use_time` int(11) DEFAULT NULL COMMENT '使用時間',
  `add_time` int(11) DEFAULT NULL COMMENT '获取时间',
  `expire_time` int(11) DEFAULT NULL COMMENT '失效时间',
  `type` tinyint(4) DEFAULT '1' COMMENT '普通道具',
  `use_way` varchar(64) DEFAULT NULL COMMENT '使用备注',
  `get_way` varchar(64) DEFAULT '1' COMMENT '得到方式',
  `c_id` int(11) DEFAULT NULL COMMENT '房间id',
  PRIMARY KEY (`id`),
  KEY `uid_type` (`uid`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=814 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-01 16:13:41
