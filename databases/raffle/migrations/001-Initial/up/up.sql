START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(001,'UP','Initial',''); COMMIT;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: tamago-v2-prod-other-raffle-ranklist.cdcumqgjhhxx.ap-southeast-1.rds.amazonaws.com    Database: hm_raffle
-- ------------------------------------------------------
-- Server version	5.6.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_raffle`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_raffle` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_raffle`;

--
-- Table structure for table `djmf_give_goods`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djmf_give_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `nickname` varchar(30) NOT NULL COMMENT '昵称',
  `item_ids` text NOT NULL,
  `inventory_ids` text NOT NULL,
  `receiver_user_id` int(10) unsigned NOT NULL,
  `is_gift` tinyint(4) NOT NULL COMMENT '是否是礼品',
  `status` tinyint(4) NOT NULL COMMENT '0:待处理，1:处理成功，2:回库成功，-1:处理失败',
  `author` varchar(20) NOT NULL COMMENT '作者',
  `error_msg` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台电竞魔饰品频赠送记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_goods_lottery`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_goods_lottery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `name` varchar(124) DEFAULT NULL COMMENT '实物名称',
  `phone` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `get_way` varchar(128) DEFAULT NULL COMMENT '获取方式',
  `addtime` int(11) DEFAULT NULL COMMENT '获得时间',
  `username` varchar(64) DEFAULT NULL COMMENT '用户名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_actives`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_actives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aname` varchar(50) NOT NULL,
  `app` varchar(20) NOT NULL,
  `start_time` int(11) NOT NULL,
  `end_time` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app` (`app`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_api_goods`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_api_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `third_id` int(10) unsigned NOT NULL,
  `sum` int(10) unsigned NOT NULL,
  `name` varchar(125) NOT NULL COMMENT '饰品名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `third_id` (`third_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='电竞API库存';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_goods`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(120) NOT NULL COMMENT '奖品名称',
  `goods_type` enum('thanku','goods','freebean','guard') NOT NULL COMMENT '礼物类型',
  `third_id` text NOT NULL COMMENT '第三方物品ID',
  `app` varchar(20) NOT NULL,
  `sum` int(11) NOT NULL COMMENT '发放数',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '单价',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '物品图片',
  `recycle_sum` int(11) NOT NULL COMMENT '回收数量',
  `prob` int(10) unsigned NOT NULL COMMENT '概率：数字越大概率越大',
  `play_group` int(10) unsigned NOT NULL COMMENT '抽奖分组，如：第一次、第二次分别抽哪些分组里的物品',
  `status` tinyint(4) NOT NULL COMMENT '0：未开启，1：开启',
  `sort` int(11) NOT NULL COMMENT '排序',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `app` (`app`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抽奖物品配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_invition`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_invition` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '邀请表主键',
  `uid` int(11) DEFAULT NULL COMMENT '邀请人',
  `invite_code` varchar(128) DEFAULT NULL COMMENT '邀请码',
  `new_uid` int(11) DEFAULT '0' COMMENT '被邀请者',
  `phone_number` varchar(15) DEFAULT '0' COMMENT '手机号',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT '0' COMMENT '更新时间',
  `is_achive_old` tinyint(1) DEFAULT '0' COMMENT '邀请人获取礼品状态 1开启 0关闭',
  `is_achive_new` tinyint(1) DEFAULT '1' COMMENT '被邀请人获取礼品状态 1开启 0关闭',
  `gift_name_new` varchar(255) DEFAULT '' COMMENT '新用户获取礼品名称',
  `gift_name_old` varchar(255) DEFAULT NULL COMMENT '邀请主人获取礼品名称',
  PRIMARY KEY (`id`),
  KEY `new_uid` (`new_uid`,`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邀请表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_retry`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_retry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(20) NOT NULL,
  `uid` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0:未发送，1:发送成功，-1:补发失败',
  `error_msg` varchar(50) NOT NULL,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='饰品补发表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_0` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_10` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_11` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_12` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_13` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_14` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_15` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_16` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_17` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_18` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_19` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_20` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_21` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_22` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_23` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_24` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_25` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_26` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_27` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_28` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_29` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_3` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_30` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_31` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_32`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_32` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_33`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_33` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_34`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_34` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_35`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_35` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_36`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_36` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_37`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_37` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_38`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_38` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_39`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_39` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_4` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_40`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_40` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_41`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_41` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_42`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_42` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_43`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_43` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_44`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_44` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_45`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_45` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_46`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_46` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_47`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_47` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_48`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_48` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_49`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_49` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_5` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_6` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_7` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_8` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raffle_users_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raffle_users_9` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `third_uid` varchar(15) NOT NULL COMMENT '第三方UID',
  `third_inventory_id` varchar(25) NOT NULL COMMENT '第三方清单(背包)ID',
  `app` varchar(20) NOT NULL,
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `goods_type` varchar(20) NOT NULL,
  `goods_name` varchar(120) NOT NULL COMMENT '物品名称',
  `sum` int(10) unsigned NOT NULL COMMENT '数量',
  `unit` int(10) unsigned NOT NULL COMMENT '单位',
  `price` float NOT NULL COMMENT '价格',
  `rarity` varchar(25) NOT NULL COMMENT '稀有度',
  `img` varchar(255) NOT NULL COMMENT '图片',
  `status` enum('inbag','used','recycled','registered') NOT NULL COMMENT 'inbag：在背包中， used：已使用，recycled：已回收，registered：已激活steam或下注',
  `get_time` int(10) unsigned NOT NULL COMMENT '获取时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '变更状态后的更新时间',
  `ip` varchar(15) NOT NULL COMMENT '客户端IP',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户抽奖表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-01 16:19:24
