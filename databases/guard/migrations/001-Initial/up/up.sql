START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(001,'UP','Initial',''); COMMIT;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-money-slave.tamago.db    Database: hm_guard
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_guard`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_guard` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_guard`;

--
-- Table structure for table `hm_anchor_guard_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_13` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_14` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_16` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_17` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_18` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_19` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_20` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_21` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_22` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_23` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_24` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_25` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_26` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_27` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_28` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_29` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_30` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_31` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_guard_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_guard_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_guard_record`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_guard_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `anchor_id` int(11) DEFAULT NULL COMMENT '主播id',
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `day` int(4) DEFAULT NULL COMMENT '天数',
  `type` int(11) DEFAULT NULL COMMENT '1骑士 2王子',
  `explain` text COMMENT '解释',
  `get_way` tinyint(4) DEFAULT '4' COMMENT '1白名单,2后台添加，3活动奖励,4正常充值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1010 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_13` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_14` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_16` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_17` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_18` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_19` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_20` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_21` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_22` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_23` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_24` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_25` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_26` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_27` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_28` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_29` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_30` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_31` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_uid_guard_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_uid_guard_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_id` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL COMMENT '过期时间',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 骑士  2王子',
  PRIMARY KEY (`id`),
  KEY `uid_expire` (`uid`,`expire`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='用户下所有守护的主播';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-01 16:17:21
