START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(003,'UP','add_deleted_from_tx_cdn',''); COMMIT;
alter table `hm_tape` add `deleted_from_tx_cdn` boolean default false;
ALTER TABLE `hm_tape` ADD INDEX `tape_deleted_from_tx_cdn_end_time` (`deleted_from_tx_cdn`, `end_time`);
