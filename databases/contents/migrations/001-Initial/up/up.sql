START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(001,'UP','Initial',''); COMMIT;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-admin-contetns-slave.tamago.db    Database: hm_contents
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_contents`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_contents` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_contents`;

--
-- Table structure for table `error_crontab`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `error_crontab` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `message` text,
  `type` enum('pay') NOT NULL COMMENT '该消息的类型',
  `retry_count` tinyint(4) DEFAULT '0' COMMENT '重试次数',
  `stat` enum('fail','success') DEFAULT 'fail' COMMENT '该消息最终的处理状态,success：表示成功；fail:表示失败',
  `createtime` int(11) DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(11) DEFAULT '0' COMMENT '更新时间',
  `error_msg` varchar(255) DEFAULT NULL COMMENT '错误原因',
  `msg_id` char(50) DEFAULT NULL COMMENT '该条消息的唯一标示；对于订单那么唯一标示就是订单号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_activce_random_gift_record`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_activce_random_gift_record` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(32) DEFAULT NULL COMMENT '订单号',
  `gift_id` int(3) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL COMMENT '用户UID',
  `cid` int(11) DEFAULT NULL COMMENT '主播CID',
  `count` int(11) DEFAULT NULL COMMENT '礼物数量',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_active_star`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_active_star` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `type` varchar(12) NOT NULL COMMENT '类型',
  `name` varchar(50) NOT NULL COMMENT '选手名称',
  `images` varchar(100) NOT NULL,
  `real_count` int(11) NOT NULL DEFAULT '0' COMMENT '真是支持人数',
  `admin_count` int(11) NOT NULL DEFAULT '0' COMMENT '后台添加人数',
  `uid` text COMMENT '支持者uid',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='嘉年华活动支持表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_advert`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_advert` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '广告表主键',
  `country_iso` varchar(100) DEFAULT NULL,
  `advert_code` varchar(128) DEFAULT NULL COMMENT '广告码',
  `title` varchar(255) DEFAULT NULL COMMENT '广告标题',
  `link` varchar(128) DEFAULT NULL COMMENT '连接',
  `order` mediumint(9) DEFAULT '100' COMMENT '权重',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片路径',
  `opusername` varchar(50) DEFAULT NULL COMMENT '上一次操作的用户',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `starttime` int(11) DEFAULT '0' COMMENT '开始时间',
  `endtime` int(11) DEFAULT '0' COMMENT '结束时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '开启状态 1开启 0关闭',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `mobile_title` varchar(255) NOT NULL COMMENT '移动端页面title',
  `area` tinyint(1) DEFAULT '0' COMMENT '区域 0全平台 1分类 2标签 3房间',
  `contents` varchar(600) DEFAULT NULL COMMENT '备注',
  `seat` tinyint(4) DEFAULT '0' COMMENT '0为非定位',
  `exposure` varchar(128) DEFAULT NULL COMMENT '曝光链接',
  `less_version` varchar(128) DEFAULT NULL COMMENT '用于移动端：小于等于这个版本号才显示',
  `more_version` varchar(128) DEFAULT NULL COMMENT '用于移动端：大于等于这个版本号才显示',
  PRIMARY KEY (`id`),
  KEY `area` (`area`),
  KEY `advert_code_st` (`advert_code`,`status`,`starttime`,`endtime`)
) ENGINE=InnoDB AUTO_INCREMENT=3908 DEFAULT CHARSET=utf8 COMMENT='广告表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_advert_rel`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_advert_rel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rid` char(20) DEFAULT NULL,
  `aid` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1' COMMENT '1：分类；2：标签；3：房间',
  `status` tinyint(1) DEFAULT '1' COMMENT '开启状态：1：默认开启；0：表示关闭',
  `opusername` char(50) DEFAULT NULL COMMENT '操作人',
  `updatetime` int(13) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rid_type_index` (`rid`,`type`,`aid`),
  KEY `status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间广告，分类广告，标签广告';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_advertcategory`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_advertcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '广告位分类表主键',
  `advert_name` varchar(255) DEFAULT NULL COMMENT '广告位分类名',
  `advert_code` varchar(128) DEFAULT NULL COMMENT '广告码',
  `type` enum('2','1') DEFAULT '1' COMMENT '1可删除，2不可删除',
  `status` smallint(6) DEFAULT '1' COMMENT '状态 1 使用  2不使用',
  `modulename` enum('common','event','link') DEFAULT 'common' COMMENT '板块名称：link：代表连接后台；event：代表赛事',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='广告位分类表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_advertcategory_bak`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_advertcategory_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '广告位分类表主键',
  `advert_name` varchar(255) DEFAULT NULL COMMENT '广告位分类名',
  `advert_code` varchar(128) DEFAULT NULL COMMENT '广告码',
  `type` enum('2','1') DEFAULT '1' COMMENT '1可删除，2不可删除',
  `status` smallint(6) DEFAULT '1' COMMENT '状态 1 使用  2不使用',
  `modulename` enum('common','event','link') DEFAULT 'common' COMMENT '板块名称：link：代表连接后台；event：代表赛事',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告位分类表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT '后台审核管理员账号id',
  `uid` int(11) unsigned NOT NULL COMMENT '用户ID',
  `realname` varchar(50) NOT NULL COMMENT '真实姓名（兼容英文）',
  `gender` tinyint(1) NOT NULL COMMENT '性别（默认0：表示未知，1男2女）',
  `gid` int(11) unsigned NOT NULL COMMENT '直播游戏',
  `qq` varchar(20) NOT NULL COMMENT 'qq号码',
  `area_code` varchar(10) NOT NULL COMMENT '区号',
  `mobile` varchar(20) NOT NULL COMMENT '手机号码',
  `identity_type` tinyint(3) unsigned NOT NULL COMMENT '身份类型',
  `identity_code` varchar(100) NOT NULL COMMENT '证件号码',
  `identity_pic` varchar(255) NOT NULL COMMENT '手持证件照片',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '状态',
  `desc_text` varchar(255) NOT NULL DEFAULT '' COMMENT '注释',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `check_time` int(11) NOT NULL COMMENT '审核时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主播表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_record_2017`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_record_2017` (
  `pid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `cid` int(11) unsigned NOT NULL COMMENT '房间号ID',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '游戏分类id',
  `channel` varchar(255) NOT NULL COMMENT '房间名称',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `date` varchar(32) DEFAULT '0' COMMENT '直播日期',
  PRIMARY KEY (`pid`),
  KEY `start_time` (`start_time`,`end_time`),
  KEY `cid` (`cid`),
  KEY `gid` (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主播个人中心';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_record_2018`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_record_2018` (
  `pid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `cid` int(11) unsigned NOT NULL COMMENT '房间号ID',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '游戏分类id',
  `channel` varchar(255) NOT NULL COMMENT '房间名称',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `date` varchar(32) DEFAULT '0' COMMENT '直播日期',
  PRIMARY KEY (`pid`),
  KEY `start_time` (`start_time`,`end_time`),
  KEY `cid` (`cid`),
  KEY `gid` (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主播个人中心';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_anchor_record_2019`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_anchor_record_2019` (
  `pid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `cid` int(11) unsigned NOT NULL COMMENT '房间号ID',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '游戏分类id',
  `channel` varchar(255) NOT NULL COMMENT '房间名称',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `date` varchar(32) DEFAULT '0' COMMENT '直播日期',
  PRIMARY KEY (`pid`),
  KEY `start_time` (`start_time`,`end_time`),
  KEY `cid` (`cid`),
  KEY `gid` (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主播个人中心';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_area`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_area` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(120) NOT NULL DEFAULT '',
  `order` smallint(5) unsigned NOT NULL DEFAULT '0',
  `is_open` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态：1：表示默认开启；0：表示关闭',
  `domain` varchar(10) NOT NULL,
  `module` char(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`pid`,`order`),
  KEY `is_open` (`is_open`,`domain`,`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_autorecommend`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_autorecommend` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL COMMENT '推荐房间ID',
  `gid` int(11) DEFAULT '0' COMMENT '游戏分类',
  `recommendcode` varchar(100) DEFAULT NULL COMMENT '开始推荐位关系码(all_recommend 推荐到所有推荐位)',
  `recommendcode_end` varchar(100) DEFAULT NULL COMMENT '结束推荐位关系码(all_recommend 推荐到所有推荐位)',
  `status` tinyint(1) DEFAULT '0' COMMENT '阶段 0未开始 1已开始 2已结束',
  `recommend_ways` tinyint(1) DEFAULT '1' COMMENT '开始推荐方式 0不推荐 1推荐',
  `recommend_ways_end` tinyint(1) DEFAULT '1' COMMENT '结束推荐方式 0不推荐 1推荐',
  `starttime` int(11) DEFAULT NULL COMMENT '开始时间',
  `ischecked` tinyint(1) DEFAULT NULL,
  `endtime` int(11) DEFAULT NULL COMMENT '结束时间',
  `updatetime` int(11) DEFAULT NULL,
  `opusername` varchar(50) DEFAULT NULL COMMENT '上一次操作的用户',
  `order` int(9) DEFAULT '0' COMMENT '开始推荐的顺序，越大排前面',
  `end_order` int(9) DEFAULT '0' COMMENT '结束推荐的顺序，越大排前面',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自动推荐表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_b_special`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_b_special` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_badge`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_badge` (
  `bid` mediumint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '徽章ID',
  `name` varchar(100) DEFAULT NULL COMMENT '徽章名称',
  `img` varchar(255) DEFAULT NULL COMMENT '徽章地址',
  `coin` decimal(9,1) DEFAULT '0.0' COMMENT '猫币：如果为0：表示关闭，为正数表示开启',
  `bean` mediumint(9) DEFAULT '0' COMMENT '猫豆',
  `opusername` varchar(50) DEFAULT NULL COMMENT '最后操作人',
  `optime` int(11) DEFAULT NULL COMMENT '最后一次操作的时间',
  `catid` mediumint(5) DEFAULT '0' COMMENT '所属的分类ID；比如生肖、充值、特级VIP等等',
  `userange` enum('allroom','specifyrooms','game','specialpage') DEFAULT 'allroom' COMMENT '使用范围',
  `sort` int(8) DEFAULT '0' COMMENT '权重',
  `hot_status` tinyint(2) DEFAULT '0' COMMENT '0 热门关闭   1开启热门',
  `hot_title` varchar(128) DEFAULT NULL COMMENT '热门标题',
  `hot_start` int(11) DEFAULT NULL COMMENT '热门开启时间',
  `hot_end` int(11) DEFAULT NULL COMMENT '热门关闭时间',
  `show_type` tinyint(2) DEFAULT '0' COMMENT '0 不显示  1显示',
  `click_type` tinyint(2) DEFAULT '0' COMMENT '点击类型 1外链接 2弹出弹窗 3房间号',
  `click_url` varchar(128) DEFAULT '' COMMENT '点击后执行的操作',
  `category_id` int(11) NOT NULL COMMENT '徽章分类id',
  `category_son_id` int(11) DEFAULT NULL COMMENT '徽章分类id 子类',
  `addtime` int(11) NOT NULL COMMENT '创建时间',
  `desc` varchar(255) DEFAULT NULL COMMENT '徽章说明',
  `img_size` varchar(32) DEFAULT NULL COMMENT '图片尺寸',
  PRIMARY KEY (`bid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_badge_category`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_badge_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL COMMENT '分类名称',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父类id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_badgerelation`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_badgerelation` (
  `cid` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `bid` mediumint(9) DEFAULT NULL COMMENT '徽章ID',
  `typeid` varchar(50) DEFAULT NULL,
  `userange` varchar(50) DEFAULT NULL COMMENT '使用的范围',
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_cachemanage`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_cachemanage` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `cnname` varchar(250) DEFAULT NULL COMMENT '缓存的中文名称',
  `createtime` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `rediskeyname` varchar(150) DEFAULT NULL COMMENT 'rediskey的名称，注意可以表示key的前缀',
  `redis_typename` varchar(50) DEFAULT NULL COMMENT '保存的位置，最终映射成为redis的配置信息',
  `cdn_typename` varchar(50) DEFAULT NULL COMMENT 'cdn的类型,页面缓存抑或json缓存',
  `cdn_keyname` text COMMENT 'cdnkey的名称，注意可以表示key的前缀',
  `updatetime` datetime DEFAULT NULL COMMENT '最后操作时间',
  `opusername` varchar(50) DEFAULT NULL COMMENT '最后操作的人',
  `is_show` tinyint(1) DEFAULT '1' COMMENT '是否显示：1：显示：0：不显示',
  `redirecturl` text COMMENT '跳转地址',
  `rediskey_suffixparams` text COMMENT 'redis使用的后缀',
  `cdnkey_suffixparams` text COMMENT 'cdn的后缀,使用json数组[]的方式',
  `cdn_cache_seperator` char(20) DEFAULT '_' COMMENT '默认的key和参数之间的分隔符',
  `redis_cache_seperator` char(20) DEFAULT NULL,
  `order` int(11) DEFAULT '0' COMMENT '排序，默认0',
  `action` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ename` (`rediskeyname`) USING BTREE COMMENT '缓存英文名唯一索引',
  UNIQUE KEY `cname` (`cnname`) USING BTREE COMMENT '缓存英文名唯一索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel` (
  `id` int(13) NOT NULL COMMENT 'id| 房间号',
  `room_number` varchar(10) DEFAULT NULL,
  `channel` varchar(50) NOT NULL COMMENT '标题|房间标题',
  `gid` int(13) NOT NULL COMMENT '游戏类型|游戏类型ID',
  `eid` int(11) DEFAULT '0' COMMENT '赛事ID',
  `uid` int(13) NOT NULL COMMENT '用户ID|用户UID',
  `tel` varchar(20) DEFAULT '',
  `username` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `content` text COMMENT '简介|房间简介',
  `is_gf` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否官方',
  `stream` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '流名称|房间流名称',
  `score_total` int(10) NOT NULL DEFAULT '0' COMMENT '总分',
  `score_count` int(10) NOT NULL DEFAULT '0' COMMENT '打分次数',
  `logo` varchar(100) NOT NULL COMMENT 'LOGO',
  `videos` int(13) NOT NULL DEFAULT '0',
  `has_ad` tinyint(2) NOT NULL DEFAULT '0',
  `ad_cnt` varchar(200) DEFAULT '',
  `views` int(13) NOT NULL DEFAULT '0' COMMENT '观看人数|房间观看人数',
  `addtime` int(13) NOT NULL DEFAULT '0',
  `updatetime` int(11) DEFAULT '0' COMMENT '修改时间',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `is_live` tinyint(1) DEFAULT '0' COMMENT '是否正在直播|房间是否在直播',
  `live_last_start_time` int(10) DEFAULT '0' COMMENT '最后一次开始时间|最后一次直播时间',
  `is_index_top` tinyint(2) DEFAULT '0' COMMENT '首页中间大播放器',
  `is_index` tinyint(2) DEFAULT '0' COMMENT '是否首页显示',
  `sh_zt` tinyint(2) DEFAULT '2' COMMENT '审核状态2 成功 1审核中  3审核不通过|房间审核状态',
  `sh_bz` varchar(200) DEFAULT '' COMMENT '审核备注',
  `sh_uname` varchar(50) DEFAULT '' COMMENT '审核者用户名',
  `sh_time` int(11) DEFAULT '0' COMMENT '审核时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除:0:表示正常；1：表示已删除',
  `del_time` int(11) DEFAULT '0' COMMENT '删除时间',
  `is_sub` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否推荐订阅',
  `subscribe` int(11) NOT NULL DEFAULT '0',
  `is_tuijian` tinyint(2) DEFAULT '0' COMMENT '小编推荐',
  `tj_title` varchar(150) DEFAULT NULL COMMENT '推荐标题',
  `tj_pic` varchar(255) DEFAULT '' COMMENT '推荐图片',
  `stream_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '房间流key',
  `ip_show` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否限制IP范围',
  `scope` varchar(200) NOT NULL DEFAULT '' COMMENT '限制区域',
  `is_new` tinyint(4) NOT NULL DEFAULT '1' COMMENT '房间切换新旧',
  `push_time` int(11) NOT NULL DEFAULT '0',
  `push_live_time` int(10) NOT NULL DEFAULT '0' COMMENT '直播推送的修改时间，当直播状态改变时需要置为0',
  `push_info_time` int(10) NOT NULL DEFAULT '0' COMMENT '直播间信息修改时改值改为0',
  `is_push` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否推送给天猫1推送',
  `top` tinyint(4) DEFAULT '0',
  `is_high` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否有超清',
  `tag` varchar(10) NOT NULL DEFAULT 'quality' COMMENT '标签属性',
  `high_status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '频率标准',
  `is_index_live` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否推荐到首页直播',
  `is_index_type` tinyint(1) NOT NULL COMMENT '是否推荐到首页所属类别',
  `is_sign` tinyint(1) NOT NULL DEFAULT '0' COMMENT '主播是否签约',
  `percent` varchar(20) NOT NULL DEFAULT '0' COMMENT '分成比例',
  `note` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '粉丝备注名称',
  `is_zhuanma` enum('no','yes') DEFAULT 'no' COMMENT '是否转码：默认no',
  `superstar_info` text COMMENT '明星信息',
  `image` varchar(200) DEFAULT NULL,
  `is_event` enum('yes','no') DEFAULT 'no' COMMENT '是否为赛事',
  `event_starttime` int(11) DEFAULT '0' COMMENT '临时：赛事开始时间',
  `event_endtime` int(11) DEFAULT '0' COMMENT '赛事结束时间',
  `roomadmin_num` tinyint(1) NOT NULL DEFAULT '10' COMMENT '房管数量',
  `opusername` varchar(50) NOT NULL COMMENT '操作者',
  `is_replay` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否开启重播',
  `is_entertainment` enum('no','yes') DEFAULT 'no' COMMENT '是否娱乐主播：默认no',
  `area` varchar(8) DEFAULT NULL,
  `is_auto_vod` enum('no','yes') DEFAULT 'no',
  `share_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`stream`),
  UNIQUE KEY `uid` (`uid`) USING BTREE,
  UNIQUE KEY `id` (`id`) USING BTREE,
  UNIQUE KEY `room_number` (`room_number`),
  KEY `stream` (`stream`),
  KEY `is_del` (`is_del`),
  KEY `status` (`status`),
  KEY `sh_zt` (`sh_zt`),
  KEY `is_live` (`is_live`),
  KEY `is_index` (`is_index`),
  KEY `gid` (`gid`),
  KEY `is_push` (`is_push`),
  KEY `tag` (`tag`),
  KEY `is_sign` (`is_sign`),
  KEY `area` (`area`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_area`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `province` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `room_number` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_attach1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_attach1` (
  `cid` int(11) NOT NULL,
  `clarity_openstat` tinyint(1) DEFAULT '1' COMMENT '是否开启未登录只能标清：1：关闭；2：开启（未登录只能观看超清）',
  `flash_chartleft` text COMMENT 'flash所用的贴图：左侧和右侧',
  `flash_chartright` varchar(255) DEFAULT NULL COMMENT 'flash所用的背景图',
  `yl_tj_pic` varchar(255) DEFAULT NULL COMMENT '娱乐推荐图',
  `yl_tj_msg` varchar(255) DEFAULT NULL COMMENT '娱乐推荐语',
  `unionid` int(11) DEFAULT '0' COMMENT '工会id：如果不为0；表示有所属工会，否则：表示没有工会',
  `entertainment_time` int(11) DEFAULT '0' COMMENT '娱乐主播修改时间',
  `yl_pc_index_info` text COMMENT '首页娱乐推荐信息',
  `search_words` varchar(255) DEFAULT NULL COMMENT '关键字搜索词',
  `match_index_info` text NOT NULL COMMENT '赛事播放器推荐信息',
  `is_join` enum('yes','no') DEFAULT 'no' COMMENT '是否加入公会后台',
  `no_cash` enum('yes','no') DEFAULT 'no' COMMENT '禁止提现',
  `cover_image` varchar(255) DEFAULT '' COMMENT '覆盖到列表上面的图片',
  `list_a_n_color` varchar(255) DEFAULT NULL COMMENT '列表页主播昵称的颜色；为空表示没有',
  `is_duanbo` tinyint(1) DEFAULT '0',
  `player_bg_img` varchar(255) DEFAULT NULL COMMENT '直播间背景图片',
  `player_bg_img_url` varchar(255) DEFAULT NULL COMMENT '直播间背景图片url',
  `notice` varchar(128) NOT NULL COMMENT '公告',
  `isExtraTab` tinyint(1) DEFAULT '0' COMMENT '是否有额外的tab',
  `extraTabTitle` varchar(255) DEFAULT '' COMMENT '额外tab的标题',
  `extraTabUrl` varchar(255) DEFAULT '' COMMENT '额外tab的跳转url',
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='channel垂直分表第一张';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_flv`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_flv` (
  `id` int(11) NOT NULL,
  `cid` int(11) DEFAULT NULL,
  `persistentId` varchar(255) DEFAULT NULL COMMENT 'flv名称',
  `streamname` varchar(11) DEFAULT NULL COMMENT '直播流名',
  `ops` int(11) DEFAULT NULL COMMENT '录制参数，包含输出格式、码率等信息。 ',
  `bucket` int(13) DEFAULT NULL COMMENT '录制存储的空间名',
  `key` varchar(255) DEFAULT NULL COMMENT '输出文件名',
  `url` varchar(255) DEFAULT NULL COMMENT '输出文件访问地址',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_join_robot`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_join_robot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `random` varchar(32) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '开启',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cid` (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=1896 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_label`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `labelname` varchar(50) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `desc` text,
  `key` varchar(255) DEFAULT NULL,
  `labeltype` mediumint(9) DEFAULT '0' COMMENT '标签类型：0：表示通用类型,默认0',
  `sort` int(11) DEFAULT '0' COMMENT '权重，越大越排前面',
  `status` tinyint(1) DEFAULT '1' COMMENT '标签开启状态：1：表示开启；0：表示不开启',
  `updatetime` int(11) DEFAULT '0' COMMENT '更新时间',
  `createtime` int(11) DEFAULT '0' COMMENT '创建时间',
  `opusername` varchar(50) DEFAULT NULL COMMENT '最后操作者',
  `labeltypecat` enum('common','game') DEFAULT 'game' COMMENT '标签类型分类：common、game等等',
  `image` varchar(255) DEFAULT NULL COMMENT '背景图片',
  `url_rule` char(50) DEFAULT NULL COMMENT '地址后缀',
  `list_stat` tinyint(1) NOT NULL DEFAULT '2' COMMENT '列表镶嵌开启or关闭：1：开启；2：关闭',
  `listweight` tinyint(1) NOT NULL DEFAULT '1' COMMENT '权重：位置',
  `listcolor` char(10) NOT NULL DEFAULT '#666666' COMMENT '默认颜色',
  `listhovercolor` char(10) NOT NULL DEFAULT '#666666' COMMENT '悬浮颜色',
  `mobile_img` varchar(255) NOT NULL,
  `index_label_order` mediumint(4) DEFAULT '0' COMMENT '首页标签推荐权重',
  `index_label_tjmsg` varchar(255) DEFAULT NULL COMMENT '首页标签推荐语',
  `index_label_tjpic` varchar(255) DEFAULT NULL COMMENT '首页标签推荐图',
  `index_label_stat` tinyint(1) DEFAULT '2' COMMENT '是否开启首页标签区：1：表示开启；2：表示关闭',
  `show_img` varchar(128) DEFAULT NULL COMMENT '标签图片',
  `rightlabeltype` mediumint(4) DEFAULT '0' COMMENT '0为默认',
  `rightlabeltypecat` enum('game','common') DEFAULT 'game' COMMENT '标签类型分类：common、game等等',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_lock`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_lock` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `cid` int(11) DEFAULT NULL COMMENT '房间id',
  `lock_time` int(11) DEFAULT NULL COMMENT '封禁时间',
  `unlock_time` int(11) DEFAULT NULL COMMENT '解封时间',
  `lock_date` int(11) DEFAULT NULL COMMENT '封禁天数',
  `lock_msg` varchar(255) DEFAULT NULL COMMENT '封禁原因',
  `status_msg` varchar(64) DEFAULT NULL COMMENT '操作信息',
  `opusername` varchar(50) DEFAULT NULL COMMENT '操作用户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3784 DEFAULT CHARSET=utf8 COMMENT='房间封禁任务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_reorder`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_reorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '主播uid',
  `type` enum('all','cat') NOT NULL DEFAULT 'all' COMMENT '列表类型all全部直播cate分类列表',
  `cat_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类id',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '开启',
  `index` int(11) NOT NULL DEFAULT '1' COMMENT '排序位置',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `updatetime` int(11) NOT NULL,
  `opusername` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `type_cat_id` (`type`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运营调整频道列表顺序';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_replay`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_replay` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `cname` varchar(255) DEFAULT NULL COMMENT '房间分类名称',
  `cid` int(11) NOT NULL COMMENT '房间cid',
  `room_number` int(11) DEFAULT NULL COMMENT '房间号',
  `channel` varchar(255) DEFAULT NULL COMMENT '房间标题',
  `flv_id` varchar(255) DEFAULT NULL COMMENT '循环播放的flv id列表',
  `opusername` varchar(255) DEFAULT NULL COMMENT '最后操作用户',
  `stream_select` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 指定回放 2 循环播放',
  `played_flv_id` int(11) DEFAULT NULL COMMENT '正在播放的flv id',
  `addtime` int(13) NOT NULL COMMENT '添加时间',
  `updatetime` int(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_roomgag`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_roomgag` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `cid` int(11) DEFAULT NULL COMMENT '房间id',
  `gag_time` int(11) DEFAULT NULL COMMENT '禁言时间',
  `ungag_time` int(11) DEFAULT NULL COMMENT '解封时间',
  `gag_date` int(11) DEFAULT NULL COMMENT '禁言天数',
  `gag_msg` varchar(255) DEFAULT NULL COMMENT '禁言原因',
  `status_msg` varchar(64) DEFAULT NULL COMMENT '禁言信息',
  `opusername` varchar(50) DEFAULT NULL COMMENT '禁言用户',
  `opusertime` int(11) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='房间禁言任务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_shop`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `room_number` varchar(20) DEFAULT NULL COMMENT '房间号',
  `cid` int(13) NOT NULL COMMENT '房间id',
  `num_iid` varchar(20) NOT NULL COMMENT '商品id',
  `item_url` varchar(255) NOT NULL COMMENT '商品url',
  `pict_url` varchar(255) NOT NULL COMMENT '商品主图片的url',
  `price` decimal(12,2) DEFAULT NULL COMMENT '商品价格',
  `title` varchar(255) NOT NULL COMMENT '商品标题',
  `is_ground` tinyint(2) NOT NULL DEFAULT '0' COMMENT '上否上架1上架0下架',
  `is_hot` tinyint(2) NOT NULL COMMENT '是否标为hot：1hot 0非hot',
  `adminControl` tinyint(1) DEFAULT '0' COMMENT '是否被禁止上架',
  `createtime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channel_tag`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channel_tag` (
  `tagid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tag_en` char(50) CHARACTER SET utf8 NOT NULL COMMENT '标签英文名称',
  `tag_cn` char(50) CHARACTER SET utf8 NOT NULL COMMENT '标签中文名称',
  `popular_rule` text COLLATE utf8_estonian_ci NOT NULL COMMENT '人气规则',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用？1：默认启用；0：禁用',
  `relationcode` char(100) CHARACTER SET utf8 NOT NULL COMMENT '关系码，唯一标示',
  PRIMARY KEY (`tagid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci COMMENT='频道所属标签表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channelstat`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channelstat` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL COMMENT '房间ID',
  `opusername` varchar(100) DEFAULT NULL,
  `updatetime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=949 DEFAULT CHARSET=utf8 COMMENT='房间的显示和隐藏';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channeltheme`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channeltheme` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `themename` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `createtime` int(11) DEFAULT NULL,
  `order` mediumint(9) DEFAULT '0' COMMENT '添加顺序',
  `channelids` text COMMENT '房间号列表',
  `recommendPos` varchar(255) DEFAULT 'index' COMMENT '推荐的位置',
  `opusername` varchar(50) DEFAULT NULL COMMENT '操作用户',
  `status` tinyint(4) DEFAULT '1' COMMENT '开启状态 1开启 2关闭',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_channelviews`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_channelviews` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cid` int(11) DEFAULT '0' COMMENT '房间号',
  `rule_no_cache` text,
  `rule_with_cache` text,
  `rule_with_superstar` text,
  `default_rule` enum('rule_with_cache','rule_with_superstar','rule_no_cache') DEFAULT NULL COMMENT '默认使用规则',
  `status` tinyint(1) DEFAULT '1' COMMENT '房间是否开启人气规则',
  `addtime` int(11) DEFAULT '0' COMMENT '规则添加时间',
  `updatetime` int(11) DEFAULT NULL,
  `opusername` varchar(50) DEFAULT NULL,
  `popular_limcat` varchar(100) DEFAULT NULL,
  `popular_tagid` tinyint(1) DEFAULT '1' COMMENT '所属分类；基础人气',
  `popular_lim_num` int(11) DEFAULT '0' COMMENT '限制的数量：0：表示没有限制',
  `is_popular_lim_select` tinyint(1) DEFAULT '0' COMMENT '是否限制；0：表示不限制；1：表示限制',
  `directshownumber` int(11) DEFAULT '0' COMMENT '外网直接显示的人气',
  `directshownumber_select` tinyint(1) DEFAULT '0' COMMENT '是否外网显示：0：表示不启用，1：表示启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14174 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_chat_lag_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_chat_lag_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `ip_address` varchar(32) DEFAULT NULL,
  `device_type` varchar(16) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=190531 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_chat_lag_log_summary`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_chat_lag_log_summary` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(11) DEFAULT NULL,
  `data1` varchar(255) DEFAULT NULL,
  `data2` int(255) DEFAULT NULL,
  `log_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_covenant`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_covenant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `anchor_uid` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `exp_time` int(11) DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `anchor_uid` (`anchor_uid`),
  KEY `add_time` (`add_time`),
  KEY `exp_time` (`exp_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='盟约表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_event_commentator`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_event_commentator` (
  `ecid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '赛事解说主键id',
  `tj_msg` varchar(255) DEFAULT NULL,
  `tj_pic` text COMMENT '推荐图',
  `cid` int(11) DEFAULT '0' COMMENT '解说的房间id，如果没有，则默认为0',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `sort` int(11) DEFAULT '0' COMMENT '顺序值',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除，1：表示删除；0：不删除',
  PRIMARY KEY (`ecid`),
  KEY `cid_index` (`cid`) USING BTREE COMMENT '使用cid进行关联'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='赛事解说的表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_event_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_event_info` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `event_id` int(12) NOT NULL COMMENT '所属赛事ID',
  `home_team` varchar(50) NOT NULL DEFAULT '' COMMENT '主队名称',
  `visiting_team` varchar(50) NOT NULL DEFAULT '' COMMENT '客队名称',
  `channel_id` int(12) NOT NULL COMMENT '频道',
  `match_time` int(10) NOT NULL DEFAULT '0',
  `end_time` int(10) NOT NULL COMMENT '比赛结束时间',
  `match_date` int(10) NOT NULL DEFAULT '0',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `addtime` int(10) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `updatetime` int(10) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `note` varchar(100) NOT NULL COMMENT '备注',
  `home_result` tinyint(4) DEFAULT NULL,
  `visiting_result` tinyint(4) DEFAULT NULL,
  `home_rate` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '主场赔率,系统每20秒更新一次',
  `visiting_rate` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '客场赔率,系统每20秒更新一次',
  `victory_team` varchar(50) NOT NULL DEFAULT '' COMMENT '胜利的队伍',
  `is_open` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否开启,默认不开启',
  `open_time` int(10) NOT NULL DEFAULT '0' COMMENT '最后操作时间',
  `is_settle` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否结算',
  `settle_time` int(10) NOT NULL DEFAULT '0' COMMENT '结算时间',
  `is_assign` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否分发仙豆,0未分发,1已分发,2回滚',
  `assign_time` int(10) NOT NULL DEFAULT '0' COMMENT '分发操作最后更新时间',
  `is_top` tinyint(2) DEFAULT '0' COMMENT '是否推流',
  `yugao_id` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '主播uid',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '去向链接',
  `home_team_id` int(11) NOT NULL COMMENT '主队的ID',
  `visiting_team_id` int(11) NOT NULL COMMENT '客队的ID',
  `game_id` int(11) DEFAULT '0' COMMENT '所属游戏类型ID，默认为当前赛事所属游戏类型ID：表示当前赛事ID',
  `last_operator` varchar(10) DEFAULT NULL COMMENT '最后操作人',
  `istop` tinyint(1) DEFAULT NULL COMMENT '是否置顶',
  PRIMARY KEY (`id`),
  KEY `yugao_id` (`yugao_id`),
  KEY `event_id` (`event_id`),
  KEY `match_date` (`match_date`,`is_del`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='赛事信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_event_list`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_event_list` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL COMMENT '赛事名称',
  `short_name` varchar(50) NOT NULL DEFAULT '' COMMENT '简称',
  `start_time` int(10) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(10) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `event_img` varchar(120) NOT NULL COMMENT '赛事图片',
  `images_2` varchar(100) NOT NULL,
  `sort` int(2) NOT NULL COMMENT '排序值',
  `is_del` tinyint(1) NOT NULL COMMENT '是否删除',
  `addtime` int(10) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `updatetime` int(10) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `note` varchar(100) NOT NULL COMMENT '备注',
  `default_cid` int(11) NOT NULL DEFAULT '0' COMMENT '默认频道',
  `url_rule` varchar(20) NOT NULL DEFAULT '' COMMENT '伪静态规则',
  `template_name` varchar(20) NOT NULL DEFAULT '' COMMENT '模板名',
  `is_top` tinyint(4) NOT NULL DEFAULT '0',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '所属游戏类型',
  `event_jd` varchar(50) DEFAULT NULL COMMENT '赛事进度',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态:1：表示启用；0：表示禁用',
  `sponsor` varchar(20) DEFAULT NULL COMMENT '主办方',
  `match_logo` varchar(120) DEFAULT NULL COMMENT '赛事logo',
  `match_day` int(11) DEFAULT NULL COMMENT '比赛天数',
  `match_mode` varchar(20) DEFAULT NULL COMMENT '比赛模式',
  `match_place` varchar(20) DEFAULT NULL COMMENT '比赛地点',
  `match_bonus` varchar(20) DEFAULT NULL COMMENT '比赛奖金',
  `match_rating` tinyint(2) DEFAULT NULL COMMENT '比赛评级',
  `last_operator` varchar(10) DEFAULT NULL COMMENT '最后操作人',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `is_del` (`is_del`),
  KEY `start_time` (`start_time`),
  KEY `end_time` (`end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='比赛赛事表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_event_race_img`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_event_race_img` (
  `id` int(13) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `image_url` varchar(255) DEFAULT NULL COMMENT '赛事对阵图',
  `eid` int(11) DEFAULT NULL COMMENT '所属赛事',
  `is_top` tinyint(1) DEFAULT NULL COMMENT '是否置顶',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `opusername` varchar(30) DEFAULT NULL COMMENT '最后操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_event_team`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_event_team` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `teamname` varchar(100) DEFAULT NULL COMMENT '队伍名称',
  `logo` varchar(250) DEFAULT NULL,
  `description` text,
  `createtime` int(11) DEFAULT NULL,
  `updatetime` int(11) DEFAULT NULL,
  `team_zone` varchar(10) DEFAULT NULL COMMENT '所属分区',
  `is_top` tinyint(1) DEFAULT NULL COMMENT '是否置顶',
  `gid` int(11) DEFAULT NULL COMMENT '游戏分类',
  `team_members` varchar(120) DEFAULT NULL COMMENT '战队成员',
  `other_members` varchar(120) DEFAULT NULL COMMENT '其他成员',
  `last_operator` varchar(10) DEFAULT NULL COMMENT '最后操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_faqfeedback`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_faqfeedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `class_name` varchar(30) DEFAULT NULL,
  `client_name` varchar(20) DEFAULT NULL COMMENT '反馈处',
  `client` varchar(20) DEFAULT NULL COMMENT '反馈类型',
  `contactway` varchar(20) DEFAULT NULL COMMENT '联系方式昵称/邮箱/手机号',
  `content` text COMMENT '反馈内容',
  `operators` varchar(20) DEFAULT NULL COMMENT '运营商',
  `city_name` varchar(20) DEFAULT '0' COMMENT '城市ID',
  `province_name` varchar(20) DEFAULT '0' COMMENT '地区ID',
  `area_name` varchar(20) DEFAULT '0' COMMENT '地方id',
  `ip` varchar(20) DEFAULT NULL COMMENT '用户IP',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `has_do` tinyint(4) DEFAULT '0' COMMENT '是否处理',
  `bz` text COMMENT '回复内容',
  `re_time` int(10) NOT NULL DEFAULT '0' COMMENT '回复时间',
  `re_name` varchar(50) DEFAULT NULL COMMENT '回复人',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户UID',
  `pic_url` varchar(200) NOT NULL DEFAULT '' COMMENT '反馈图片',
  `room_name` varchar(200) NOT NULL COMMENT '被举报人房间名称',
  `room_cid` varchar(20) NOT NULL COMMENT '被举报人房间ID',
  `report_name` varchar(100) NOT NULL COMMENT '举报人',
  `type` tinyint(4) DEFAULT '1' COMMENT '数据类型，1反馈，2举报',
  `sign` tinyint(4) DEFAULT '1' COMMENT '1未被标记 2被标记',
  `ext` text COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27702 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_gag`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_gag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '被禁用户UID',
  `cid` int(11) NOT NULL DEFAULT '0' COMMENT '房间号，无房间号即全局禁言',
  `ban_uid` int(11) NOT NULL COMMENT '禁言管理者UID',
  `addtime` int(11) NOT NULL COMMENT '禁言时间',
  `mname` varchar(50) NOT NULL COMMENT '操作员账号',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '是否永久禁言,默认1是永久',
  `is_root` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为超级管理员操作，1是0否',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1.进行中 2.已结束',
  `node` varchar(255) DEFAULT '' COMMENT '禁言原因',
  `channel` varchar(20) DEFAULT '' COMMENT '禁言渠道',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `ban_uid` (`ban_uid`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=139673 DEFAULT CHARSET=utf8 COMMENT='禁言表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_game`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_game` (
  `gid` int(13) NOT NULL AUTO_INCREMENT,
  `ename` varchar(50) NOT NULL COMMENT '英文缩写',
  `cname` varchar(50) NOT NULL COMMENT '中文名',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `desc` text COMMENT '描述',
  `key` varchar(255) DEFAULT NULL COMMENT '关键字',
  `images` varchar(100) NOT NULL COMMENT '封面图片',
  `images_2` varchar(100) NOT NULL DEFAULT '' COMMENT '同游戏的其他图片',
  `logo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '游戏小logo',
  `videos` int(13) NOT NULL DEFAULT '0',
  `lives` int(13) NOT NULL DEFAULT '0',
  `views` int(13) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `is_display` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `index_display` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否首页显示',
  `addtime` int(11) NOT NULL DEFAULT '0',
  `push_time` int(11) NOT NULL DEFAULT '0',
  `url_rule` varchar(20) NOT NULL DEFAULT '' COMMENT '伪静态规则',
  `cdn` varchar(50) NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否在专题页中显示',
  `event_logo` varchar(255) NOT NULL COMMENT '赛事logo',
  `updatetime` int(11) DEFAULT NULL COMMENT '修改时间',
  `opusername` varchar(50) DEFAULT NULL COMMENT '最后操作人',
  `is_show_tab` tinyint(1) DEFAULT '1' COMMENT '是否显示到TAB；1：表示显示，0：表示不显示；默认为1',
  `includelabels` text COMMENT '包含的标签，比如1,2,3',
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_game_channel_label`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_game_channel_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gid` int(11) DEFAULT NULL COMMENT '房间所属游戏的ID',
  `labelid` varchar(50) DEFAULT NULL COMMENT '标签ID；注意，该标签的ID和游戏所拥有的标签没有必然关系',
  `cid` int(11) DEFAULT NULL COMMENT '房间ID',
  `opusername` varchar(50) DEFAULT '' COMMENT '最后操作人',
  `updatetime` int(11) DEFAULT NULL COMMENT '最后操作时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `gid_labelid_cid` (`gid`,`labelid`,`cid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=232570 DEFAULT CHARSET=utf8 COMMENT='房间拥有的标签列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_gamecollection`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_gamecollection` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) DEFAULT NULL COMMENT '中文名称',
  `ename` varchar(50) DEFAULT NULL COMMENT '英文名称',
  `sort` mediumint(9) DEFAULT NULL COMMENT '排序',
  `includegids` varchar(255) DEFAULT NULL COMMENT '包含的游戏，格式为11,23,87,111',
  `url_rule` varchar(50) DEFAULT NULL COMMENT '地址后缀',
  `is_show_tab` tinyint(1) DEFAULT '0' COMMENT '是否显示游戏tab，默认为0：不显示；1：表示显示',
  `image` varchar(255) DEFAULT NULL COMMENT '封面',
  `createtime` int(11) DEFAULT NULL,
  `updatetime` int(11) DEFAULT NULL,
  `opusername` varchar(50) DEFAULT NULL COMMENT '最后操作者',
  `title` varchar(200) NOT NULL,
  `desc` text NOT NULL,
  `key` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类集合表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_gift_money`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_gift_money` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `cid` int(11) NOT NULL COMMENT '房间号',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `money` int(11) NOT NULL COMMENT '经验',
  `date` varchar(12) NOT NULL COMMENT '日期',
  `year` int(4) NOT NULL COMMENT '年',
  `month` int(2) NOT NULL COMMENT '月',
  `week` int(3) NOT NULL COMMENT '周',
  `type` int(3) NOT NULL COMMENT '类型',
  `addtime` varchar(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `date` (`date`),
  KEY `money` (`money`)
) ENGINE=InnoDB AUTO_INCREMENT=11232254 DEFAULT CHARSET=utf8 COMMENT='礼物经验表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_gift_money_2017_03_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_gift_money_2017_03_27` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `cid` int(11) NOT NULL COMMENT '房间号',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `money` int(11) NOT NULL COMMENT '经验',
  `date` varchar(12) NOT NULL COMMENT '日期',
  `year` int(4) NOT NULL COMMENT '年',
  `month` int(2) NOT NULL COMMENT '月',
  `week` int(3) NOT NULL COMMENT '周',
  `type` int(3) NOT NULL COMMENT '类型',
  `addtime` varchar(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `date` (`date`),
  KEY `money` (`money`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='礼物经验表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_help`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_help` (
  `id` int(13) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `link` varchar(100) DEFAULT NULL COMMENT '链接',
  `content` text COMMENT '内容',
  `begin` int(13) DEFAULT NULL COMMENT '时间',
  `weight` int(5) DEFAULT NULL COMMENT '权重',
  `uname` varchar(20) DEFAULT NULL COMMENT '管理员名字',
  `type` tinyint(2) DEFAULT '0' COMMENT '帮助类型',
  `status` int(1) DEFAULT '1' COMMENT '状态',
  `is_hot` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否常见问题',
  `type_name` tinyint(2) NOT NULL DEFAULT '2' COMMENT '类别: 1.我是主播  2.我是观众',
  `updatetime` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_help_type`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_help_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(100) NOT NULL COMMENT '帮助类型名称',
  `type` tinyint(1) NOT NULL COMMENT '类别',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帮助类型名称';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_loveliness`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_loveliness` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(10) NOT NULL COMMENT '用户uid或主播的UID',
  `cid` int(10) NOT NULL COMMENT '房间号',
  `score` int(10) NOT NULL COMMENT '粉丝或魅力值',
  `type` smallint(1) NOT NULL COMMENT '1 粉丝值 2 魅力值',
  `settime` int(10) NOT NULL COMMENT '更新时间',
  `other_uid` int(10) NOT NULL COMMENT '主播的UID',
  `is_sel` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否佩戴勋章',
  PRIMARY KEY (`id`),
  KEY `other_uid` (`other_uid`),
  KEY `uid` (`uid`),
  KEY `cid` (`cid`),
  KEY `type` (`type`),
  KEY `is_sel` (`is_sel`)
) ENGINE=InnoDB AUTO_INCREMENT=532991 DEFAULT CHARSET=utf8 COMMENT='粉丝或魅力表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_newcategory`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_newcategory` (
  `id` int(13) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `category_name` varchar(20) NOT NULL COMMENT '新闻类型',
  `order` mediumint(6) DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1开,0关',
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_newscontent`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_newscontent` (
  `id` int(13) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(50) DEFAULT NULL COMMENT '新闻标题',
  `link` varchar(100) DEFAULT NULL COMMENT '链接',
  `content` text COMMENT '内容',
  `createtime` int(11) DEFAULT NULL COMMENT '开始日期',
  `weight` int(5) DEFAULT NULL COMMENT '权重 ---暂时弃用',
  `uid` int(13) DEFAULT NULL COMMENT '管理员ID',
  `uname` varchar(20) DEFAULT NULL COMMENT '管理员名字',
  `type` tinyint(2) DEFAULT '0' COMMENT '新闻类型',
  `desc` text COMMENT '摘要 暂时弃用',
  `key` varchar(255) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '状态0：表示禁止；1：表示开启',
  `is_hot` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'HOT',
  `showtype` enum('web','qqgame') DEFAULT 'web' COMMENT '显示的位置，默认网站',
  `is_top` tinyint(1) DEFAULT NULL,
  `rel_content` text,
  `top_weight` smallint(4) DEFAULT NULL,
  `rel_game` varchar(255) DEFAULT NULL,
  `rel_match` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `type` (`type`),
  KEY `showtype` (`showtype`),
  KEY `createtime` (`createtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_reccatcontpos`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_reccatcontpos` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort` int(11) DEFAULT NULL,
  `recommendcode` varchar(50) DEFAULT NULL,
  `rctcid` int(11) DEFAULT NULL COMMENT '推荐分类内容ID',
  `createtime` int(11) DEFAULT NULL,
  `updatetime` int(11) DEFAULT NULL,
  `opusername` varchar(50) DEFAULT NULL COMMENT '操作者',
  `recommendname` varchar(50) DEFAULT NULL COMMENT '推荐位名称，这里做一个冗余，方便查询使用,它和recommendcode对应',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='推荐分类推荐位表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_reccattype`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_reccattype` (
  `rctid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cname` varchar(255) DEFAULT NULL COMMENT '中文名称',
  `ename` varchar(50) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL COMMENT '权重',
  `status` tinyint(1) DEFAULT '1' COMMENT '默认1；1：表示开启；0：表示关闭',
  `createtime` int(11) DEFAULT NULL,
  `updatetime` int(11) DEFAULT NULL,
  `opusername` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`rctid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='推荐分类类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_reccattypecont`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_reccattypecont` (
  `rctcid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` int(11) DEFAULT NULL COMMENT '推荐分类类型ID',
  `status` tinyint(1) DEFAULT '1' COMMENT '1：表示开启；0：表示关闭',
  `cname` varchar(255) DEFAULT NULL COMMENT '名称',
  `url` text COMMENT '连接',
  `image` varchar(255) DEFAULT NULL COMMENT '封面',
  `sort` int(11) DEFAULT '0',
  `createtime` int(11) DEFAULT NULL,
  `updatetime` int(11) DEFAULT NULL,
  `opusername` varchar(50) DEFAULT NULL,
  `typename` varchar(255) DEFAULT NULL COMMENT '推荐类型',
  `targettype` tinyint(1) DEFAULT NULL COMMENT '1：分类 2：标签 3：集合 4：房间 5：h5',
  PRIMARY KEY (`rctcid`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='推荐分类内容表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_recommendcategory`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_recommendcategory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `recommendname` varchar(200) DEFAULT NULL COMMENT '推荐位名称',
  `recommendcode` varchar(100) DEFAULT NULL COMMENT '推荐码',
  `status` tinyint(1) DEFAULT '1' COMMENT '是否有效：1：有效；0：表示无效',
  `recommendclass` enum('event','channel','reccatcont') DEFAULT 'event' COMMENT '推荐码所属类型',
  `remark` varchar(160) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `relationcode` (`recommendcode`) USING BTREE COMMENT '关系码'
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_recommendchannel`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_recommendchannel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(11) DEFAULT NULL COMMENT '推荐房间ID',
  `gid` int(11) DEFAULT '0' COMMENT '游戏分类',
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `eid` int(11) DEFAULT '0' COMMENT '赛事ID',
  `recommendcode` varchar(100) DEFAULT NULL COMMENT '推荐位关系码',
  `remark` varchar(300) DEFAULT NULL COMMENT '备注信息',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL,
  `opusername` varchar(50) DEFAULT NULL COMMENT '上一次操作的用户',
  `order` mediumint(9) DEFAULT '0' COMMENT '推荐的顺序，越大排前面',
  `ext` varchar(255) DEFAULT NULL COMMENT '扩展字段',
  `img` varchar(255) DEFAULT NULL COMMENT '推荐图片',
  `exposure` varchar(255) NOT NULL COMMENT '曝光',
  PRIMARY KEY (`id`),
  KEY `index_recommendcode` (`recommendcode`)
) ENGINE=InnoDB AUTO_INCREMENT=14731 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_recommendevent`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_recommendevent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(11) DEFAULT NULL COMMENT '推荐内容ID',
  `recommendcode` varchar(100) DEFAULT NULL COMMENT '推荐位关系码',
  `remark` varchar(300) DEFAULT NULL COMMENT '备注信息',
  `opusername` varchar(50) DEFAULT NULL COMMENT '上一次操作的用户',
  `order` mediumint(9) DEFAULT '0' COMMENT '推荐的顺序，越大排前面',
  `linkurl` varchar(128) DEFAULT NULL COMMENT '推荐链接',
  `images` varchar(255) DEFAULT NULL COMMENT '推荐图片',
  `ext` varchar(255) DEFAULT NULL COMMENT '扩展字段',
  `addtime` int(11) DEFAULT '0' COMMENT '添加时间',
  `updatetime` int(11) DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_rediskeys`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_rediskeys` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `cnname` varchar(250) DEFAULT NULL,
  `enname` varchar(150) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `type` enum('channel','user','match','complex','event') DEFAULT 'complex' COMMENT '综合',
  `savelocation` varchar(50) DEFAULT NULL COMMENT '保存的位置，最终映射成为redis的配置信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ename` (`enname`) USING BTREE COMMENT '缓存英文名唯一索引',
  UNIQUE KEY `cname` (`cnname`) USING BTREE COMMENT '缓存英文名唯一索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_report`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` varchar(11) NOT NULL COMMENT '举报用户ID',
  `report_uid` varchar(11) NOT NULL COMMENT '被举报用户ID',
  `content` varchar(100) NOT NULL COMMENT '举报内容',
  `channel_id` varchar(11) NOT NULL COMMENT '房间号',
  `gift` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否送豆 0未送 1待送 2已送',
  `is_del` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `action` tinyint(2) NOT NULL DEFAULT '0' COMMENT '禁言动作 0未处理，1房间永久, 2.房间临时 3全站永久 4作废,5警告',
  `addtime` varchar(15) NOT NULL COMMENT '添加时间',
  `updatetime` varchar(15) NOT NULL COMMENT '修改时间',
  `note` varchar(50) NOT NULL DEFAULT '0' COMMENT '备注',
  `opusername` varchar(50) DEFAULT NULL COMMENT '最后操作人',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `report_uid` (`report_uid`),
  KEY `channel_id` (`channel_id`),
  KEY `action` (`action`),
  KEY `is_del` (`is_del`),
  KEY `addtime` (`addtime`)
) ENGINE=InnoDB AUTO_INCREMENT=592407 DEFAULT CHARSET=utf8 COMMENT='举报用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_room_number`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_room_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `room_number` int(10) unsigned NOT NULL COMMENT '房间号',
  `is_special` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为靓号，默认0不是1是',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已发放，默认0未发放1已发放',
  `update_time` int(11) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `room_number` (`room_number`),
  KEY `is_special` (`is_special`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2200812 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_share_active`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_share_active` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL COMMENT '活动id',
  `share_title` varchar(255) DEFAULT NULL COMMENT '分享标题',
  `share_content` text,
  `share_image` varchar(255) DEFAULT NULL COMMENT '分享图片地址',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '活动类型1：广告表的活动 2：移动端活动表的活动',
  `opusername` varchar(50) DEFAULT NULL COMMENT '操作人',
  `active_type` varchar(255) NOT NULL COMMENT '活动类型',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_shield_list`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_shield_list` (
  `sid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `uid` int(11) NOT NULL COMMENT '用户uid',
  `ban_acount` text NOT NULL COMMENT '被屏蔽用户uid',
  `addtime` varchar(15) NOT NULL COMMENT '添加时间',
  `updatetime` varchar(15) NOT NULL COMMENT '修改时间',
  `note` varchar(50) NOT NULL DEFAULT '0' COMMENT '备注',
  PRIMARY KEY (`sid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='聊天屏蔽用户列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2016_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2016_10` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2016_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2016_11` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2016_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2016_12` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2016_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2016_5` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2016_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2016_6` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  `adddate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2016_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2016_7` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  `adddate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2016_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2016_8` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2016_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2016_9` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_10` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_11` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=38218 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_12` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1725 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_3` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_4` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_5` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_6` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_7` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_8` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2017_9` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2018_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2018_1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3843 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2018_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2018_2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=6072 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2018_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2018_3` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=6177 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2018_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2018_4` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5102 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2018_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2018_5` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5401 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2018_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2018_6` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5552 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2018_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2018_7` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5029 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_signin_2018_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_signin_2018_8` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户UID',
  `punch` text NOT NULL COMMENT '签到天数',
  `month` int(11) NOT NULL COMMENT '月份',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `max_day` int(3) NOT NULL DEFAULT '0' COMMENT '连续签到最大天数',
  `conn_day` int(11) NOT NULL DEFAULT '0' COMMENT '签到最新连续值',
  `day_3` int(11) NOT NULL DEFAULT '0' COMMENT '连续三天签到',
  `day_7` int(11) NOT NULL DEFAULT '0' COMMENT '连续七天签到',
  `day_15` int(11) NOT NULL DEFAULT '0' COMMENT '连续十五天签到',
  `day_30` int(11) NOT NULL DEFAULT '0' COMMENT '满月签到',
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `month` (`month`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=445 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_stream_do_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_stream_do_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(255) NOT NULL,
  `on_off` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0关闭，1开启',
  `why` varchar(255) DEFAULT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stream_name` (`stream_name`),
  KEY `on_off` (`on_off`)
) ENGINE=InnoDB AUTO_INCREMENT=7220 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_stream_kill_list`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_stream_kill_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(10) NOT NULL,
  `on_off` int(1) NOT NULL,
  `why` varchar(100) NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4600 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_subscribe`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_subscribe` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` int(13) NOT NULL COMMENT '订阅用户UID',
  `channel_id` int(13) NOT NULL COMMENT '房间号',
  `time` int(11) NOT NULL COMMENT '添加时间',
  `is_send` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否推送消息',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `channel_id` (`channel_id`),
  KEY `is_send` (`is_send`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_subscribe_admin`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_subscribe_admin` (
  `id` int(13) NOT NULL AUTO_INCREMENT COMMENT 'id|',
  `cid` int(10) unsigned NOT NULL COMMENT '房间CID',
  `hand_operating` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '手动设置 1手动设置 0否',
  `type` int(1) DEFAULT '3' COMMENT '增长模式 1-5对应 x0.0， x0.5， x1.0， x2.0， x3.0',
  `rule_settings` varchar(120) DEFAULT NULL COMMENT '手动规则',
  `lasttime` int(11) DEFAULT NULL COMMENT '最后时间',
  `opusername` varchar(50) DEFAULT NULL COMMENT '操作者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_sys_config`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_sys_config` (
  `variable` varchar(50) NOT NULL COMMENT '变量',
  `value` text COMMENT '值',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  PRIMARY KEY (`variable`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统配置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_sys_message`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_sys_message` (
  `id` int(13) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(32) NOT NULL COMMENT '标题',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '内容',
  `type` int(1) DEFAULT '1' COMMENT '类型 1主播、2用户',
  `sys_stage` int(1) DEFAULT '1' COMMENT '发布阶段 1未发布、2已发布、3取消',
  `send_scope` text COMMENT '范围(全体、自定义、分类)',
  `sendtime` int(11) DEFAULT NULL COMMENT '发布时间',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `opusername` varchar(50) DEFAULT NULL COMMENT '操作者',
  `count` varchar(11) DEFAULT NULL COMMENT '人数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=454 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_task_min`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_task_min` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户id',
  `record` text COMMENT '可以领取的奖励记录',
  `first_enter_time` int(11) NOT NULL COMMENT '当天首次进入房间的时间',
  `final_rec_time` int(11) NOT NULL COMMENT '最后领取的时间',
  `time` int(11) NOT NULL COMMENT '当天的时间',
  `task_time` int(11) NOT NULL DEFAULT '0',
  `task_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '任务类型',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `time` (`time`),
  KEY `task_type` (`task_type`)
) ENGINE=InnoDB AUTO_INCREMENT=21068743 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_tieba_news`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_tieba_news` (
  `id` int(13) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `news_title` varchar(50) DEFAULT NULL COMMENT '新闻标题',
  `news_link` varchar(200) DEFAULT NULL COMMENT '新闻链接',
  `news_time` int(11) DEFAULT NULL COMMENT '新闻添加时间',
  `image_url` varchar(255) DEFAULT NULL COMMENT '新闻链接',
  `eid` int(11) DEFAULT NULL COMMENT '所属赛事',
  `is_top` tinyint(1) DEFAULT NULL COMMENT '是否置顶',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `opusername` varchar(30) DEFAULT NULL COMMENT '最后操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2016_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2016_10` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2016_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2016_11` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2016_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2016_12` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2016_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2016_5` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2016_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2016_6` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2016_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2016_7` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2016_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2016_8` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2016_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2016_9` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_1` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_10` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_11` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=239970 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_12` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2932 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_2` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_3` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_4` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_5` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_6` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_7` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_8` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2017_9` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2018_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2018_1` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10851 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2018_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2018_2` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16211 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2018_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2018_3` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2018_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2018_4` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19751 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2018_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2018_5` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22965 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2018_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2018_6` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14387 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2018_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2018_7` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14471 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_usertask_2018_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_usertask_2018_8` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户的ID',
  `task_id` int(11) NOT NULL COMMENT '任务的id编号',
  `get_moneytime` int(11) NOT NULL COMMENT '领取的时间',
  `money` int(11) NOT NULL,
  `adddate` int(11) NOT NULL,
  PRIMARY KEY (`id`,`uid`),
  KEY `index_name` (`task_id`),
  KEY `uid` (`uid`),
  KEY `policy_id` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=302 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_videodemand`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_videodemand` (
  `did` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT '' COMMENT '标题',
  `subtitle` varchar(200) DEFAULT '' COMMENT '副标题',
  `gid` int(11) DEFAULT '0' COMMENT '分类ID',
  `pic` varchar(255) DEFAULT '' COMMENT '图片',
  `duration` mediumint(8) DEFAULT '0' COMMENT '时长',
  `sort` int(11) DEFAULT '0' COMMENT '权重',
  `link` char(200) DEFAULT '' COMMENT '链接地址',
  `opusername` char(100) DEFAULT '' COMMENT '最后操作人',
  `createtime` int(11) DEFAULT '0',
  `updatetime` int(11) DEFAULT '0',
  `gname` varchar(255) DEFAULT NULL COMMENT '冗余游戏名称：中文游戏名',
  PRIMARY KEY (`did`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_web_config`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_web_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cfg_type` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '线路类型：cdn，flash_ver等等',
  `code` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '线路名称',
  `val` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '线路码率',
  `foreign_low` tinyint(3) unsigned NOT NULL COMMENT '国外低权限(1:是，0:否)',
  `sort` tinyint(2) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '线路开关：1：表示开启：0：表示关闭',
  `is_heigh` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否多码率:1：是，其他：否',
  `default` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否默认线路：1：是；0：否',
  PRIMARY KEY (`id`),
  KEY `cfg_type` (`cfg_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='网站的相关配置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_weekstargift_list`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_weekstargift_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `all_week_gift_id` varchar(100) NOT NULL COMMENT '一周礼物的id 按逗号分开',
  `year` int(11) NOT NULL COMMENT '年',
  `week` int(11) NOT NULL COMMENT '一年的第几周',
  `is_auto` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否自动生成 0 否 1 是',
  `price_range` varchar(100) DEFAULT NULL,
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `opusername` varchar(50) NOT NULL COMMENT '操作人',
  PRIMARY KEY (`id`),
  KEY `weekAndYear` (`week`,`year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hm_weekstargift_record`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hm_weekstargift_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_uid` int(11) NOT NULL COMMENT '赠送用户UID',
  `anchor_uid` int(11) NOT NULL COMMENT '主播UID',
  `gift_id` int(8) NOT NULL COMMENT '礼物id',
  `amount` int(11) DEFAULT NULL COMMENT '一次赠送的数量',
  `year` int(11) DEFAULT NULL COMMENT '年',
  `week` int(11) DEFAULT NULL COMMENT '一年的第几周',
  `createtime` int(11) NOT NULL COMMENT '赠送时间',
  PRIMARY KEY (`id`),
  KEY `week_year_gift` (`week`,`year`,`gift_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jianhuang_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jianhuang_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` bigint(20) DEFAULT NULL,
  `ts` int(11) NOT NULL COMMENT '插入时间',
  `streamname` varchar(40) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL COMMENT '0:色情 1:性感 2:正常',
  `pic` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `isreview` tinyint(2) DEFAULT '0' COMMENT '是否需要复核',
  `roomNumber` varchar(20) NOT NULL DEFAULT '' COMMENT '房间号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='鉴黄记录日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_record`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `stream_id` int(10) unsigned NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `stream` varchar(255) NOT NULL COMMENT '流名称',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `len` int(11) DEFAULT '0' COMMENT '时长',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stream_id` (`stream_id`),
  KEY `start_time` (`start_time`,`end_time`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='直播记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_record_2016`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_record_2016` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `stream_id` int(10) unsigned NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `stream` varchar(255) NOT NULL COMMENT '流名称',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `len` int(11) DEFAULT '0' COMMENT '时长',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stream_id` (`stream_id`),
  KEY `start_time` (`start_time`,`end_time`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='直播记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_record_2017`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_record_2017` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `stream_id` int(10) unsigned NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `stream` varchar(255) NOT NULL COMMENT '流名称',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `len` int(11) DEFAULT '0' COMMENT '时长',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stream_id` (`stream_id`),
  KEY `start_time` (`start_time`,`end_time`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=790238 DEFAULT CHARSET=utf8 COMMENT='直播记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_record_2018`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_record_2018` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `stream_id` int(10) unsigned NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `stream` varchar(255) NOT NULL COMMENT '流名称',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `len` int(11) DEFAULT '0' COMMENT '时长',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stream_id` (`stream_id`),
  KEY `start_time` (`start_time`,`end_time`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=560880 DEFAULT CHARSET=utf8 COMMENT='直播记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_time_census_2016_05_01`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_time_census_2016_05_01` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(20) NOT NULL COMMENT '流名称',
  `room_id` int(10) NOT NULL COMMENT '房间号',
  `uid` int(10) NOT NULL,
  `time` int(10) NOT NULL COMMENT '直播的总时长',
  `valid_time` int(10) NOT NULL COMMENT '直播的有效时长',
  `mor_time` int(10) NOT NULL COMMENT '早间档有效时长',
  `valid_day` int(10) NOT NULL COMMENT '有效天数',
  `timestamp` int(10) NOT NULL COMMENT '当天时间',
  PRIMARY KEY (`id`),
  KEY `index_stream` (`stream_name`),
  KEY `index_time` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_time_census_2016_09_01`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_time_census_2016_09_01` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(20) NOT NULL COMMENT '流名称',
  `room_id` int(10) NOT NULL COMMENT '房间号',
  `uid` int(10) NOT NULL,
  `time` int(10) NOT NULL COMMENT '直播的总时长',
  `valid_time` int(10) NOT NULL COMMENT '直播的有效时长',
  `mor_time` int(10) NOT NULL COMMENT '早间档有效时长',
  `valid_day` int(10) NOT NULL COMMENT '有效天数',
  `timestamp` int(10) NOT NULL COMMENT '当天时间',
  PRIMARY KEY (`id`),
  KEY `index_stream` (`stream_name`),
  KEY `index_time` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_time_census_2017_01_01`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_time_census_2017_01_01` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(20) NOT NULL COMMENT '流名称',
  `room_id` int(10) NOT NULL COMMENT '房间号',
  `uid` int(10) NOT NULL,
  `time` int(10) NOT NULL COMMENT '直播的总时长',
  `valid_time` int(10) NOT NULL COMMENT '直播的有效时长',
  `mor_time` int(10) NOT NULL COMMENT '早间档有效时长',
  `valid_day` int(10) NOT NULL COMMENT '有效天数',
  `timestamp` int(10) NOT NULL COMMENT '当天时间',
  PRIMARY KEY (`id`),
  KEY `index_stream` (`stream_name`),
  KEY `index_time` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_time_census_2017_05_01`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_time_census_2017_05_01` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(20) NOT NULL COMMENT '流名称',
  `room_id` int(10) NOT NULL COMMENT '房间号',
  `uid` int(10) NOT NULL,
  `time` int(10) NOT NULL COMMENT '直播的总时长',
  `valid_time` int(10) NOT NULL COMMENT '直播的有效时长',
  `mor_time` int(10) NOT NULL COMMENT '早间档有效时长',
  `valid_day` int(10) NOT NULL COMMENT '有效天数',
  `timestamp` int(10) NOT NULL COMMENT '当天时间',
  PRIMARY KEY (`id`),
  KEY `index_stream` (`stream_name`),
  KEY `index_time` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `live_time_census_2017_09_01`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live_time_census_2017_09_01` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(20) NOT NULL COMMENT '流名称',
  `room_id` int(10) NOT NULL COMMENT '房间号',
  `uid` int(10) NOT NULL,
  `time` int(10) NOT NULL COMMENT '直播的总时长',
  `valid_time` int(10) NOT NULL COMMENT '直播的有效时长',
  `mor_time` int(10) NOT NULL COMMENT '早间档有效时长',
  `valid_day` int(10) NOT NULL COMMENT '有效天数',
  `timestamp` int(10) NOT NULL COMMENT '当天时间',
  PRIMARY KEY (`id`),
  KEY `index_stream` (`stream_name`),
  KEY `index_time` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stream_live_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stream_live_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stream_id` int(10) unsigned NOT NULL,
  `stream_name` varchar(10) NOT NULL,
  `stream_ser_ip` varchar(15) NOT NULL,
  `stream_start_time` int(11) NOT NULL,
  `stream_end_time` int(11) NOT NULL,
  `start_date` int(11) NOT NULL,
  `uid` int(13) NOT NULL,
  `channel_id` int(13) NOT NULL,
  `channel` varchar(50) NOT NULL,
  `gid` int(13) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stream_id` (`stream_id`),
  KEY `stream_name` (`stream_name`),
  KEY `stream_start_time` (`stream_start_time`),
  KEY `stream_end_time` (`stream_end_time`),
  KEY `start_date` (`start_date`),
  KEY `uid` (`uid`),
  KEY `channel_id` (`channel_id`),
  KEY `channel` (`channel`),
  KEY `gid` (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stream_server_ip`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stream_server_ip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ServerIp` varchar(20) NOT NULL,
  `ConnectNum` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stream_state`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stream_state` (
  `stream_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(50) NOT NULL,
  `stream_ser_ip` varchar(15) NOT NULL,
  `stream_flashver` varchar(50) NOT NULL,
  `stream_start_time` int(25) NOT NULL,
  `stream_end_time` int(25) NOT NULL,
  `stream_state` varchar(5) NOT NULL,
  `start_date` int(25) NOT NULL,
  `note` varchar(50) NOT NULL DEFAULT '0' COMMENT '备注',
  PRIMARY KEY (`stream_id`),
  KEY `stream_name` (`stream_name`),
  KEY `start_date` (`start_date`),
  KEY `stream_state` (`stream_state`),
  KEY `stream_end_time` (`stream_end_time`),
  KEY `stream_start_time` (`stream_start_time`),
  KEY `idx_stream_name_stream_state` (`stream_name`,`stream_state`)
) ENGINE=InnoDB AUTO_INCREMENT=4480110 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stream_state_2014`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stream_state_2014` (
  `stream_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(10) NOT NULL,
  `stream_ser_ip` varchar(15) NOT NULL,
  `stream_flashver` varchar(50) DEFAULT '',
  `stream_start_time` int(25) NOT NULL,
  `stream_end_time` int(25) NOT NULL,
  `stream_state` varchar(5) NOT NULL,
  `start_date` int(25) NOT NULL,
  `note` varchar(50) NOT NULL DEFAULT '0' COMMENT '备注',
  PRIMARY KEY (`stream_id`),
  KEY `stream_name` (`stream_name`),
  KEY `start_date` (`start_date`),
  KEY `stream_state` (`stream_state`),
  KEY `stream_end_time` (`stream_end_time`),
  KEY `stream_start_time` (`stream_start_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stream_state_2015`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stream_state_2015` (
  `stream_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(10) NOT NULL,
  `stream_ser_ip` varchar(15) NOT NULL,
  `stream_flashver` varchar(50) DEFAULT NULL,
  `stream_start_time` int(25) NOT NULL,
  `stream_end_time` int(25) NOT NULL,
  `stream_state` varchar(5) NOT NULL,
  `start_date` int(25) NOT NULL,
  `note` varchar(50) NOT NULL DEFAULT '0' COMMENT '备注',
  PRIMARY KEY (`stream_id`),
  KEY `stream_name` (`stream_name`),
  KEY `start_date` (`start_date`),
  KEY `stream_state` (`stream_state`),
  KEY `stream_end_time` (`stream_end_time`),
  KEY `stream_start_time` (`stream_start_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stream_state_2016`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stream_state_2016` (
  `stream_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_name` varchar(50) NOT NULL,
  `stream_ser_ip` varchar(15) NOT NULL,
  `stream_flashver` varchar(50) NOT NULL,
  `stream_start_time` int(25) NOT NULL,
  `stream_end_time` int(25) NOT NULL,
  `stream_state` varchar(5) NOT NULL,
  `start_date` int(25) NOT NULL,
  `note` varchar(50) NOT NULL DEFAULT '0' COMMENT '备注',
  PRIMARY KEY (`stream_id`),
  KEY `stream_name` (`stream_name`),
  KEY `start_date` (`start_date`),
  KEY `stream_state` (`stream_state`),
  KEY `stream_end_time` (`stream_end_time`),
  KEY `stream_start_time` (`stream_start_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_campaign`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_campaign` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Campaign Name',
  `start_date` datetime DEFAULT NULL COMMENT 'Start date',
  `end_date` datetime DEFAULT NULL COMMENT 'End date',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Campaign Record';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_critical_gift`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_critical_gift` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `gift_id` int(11) DEFAULT NULL COMMENT 'Gift ID',
  `gift_name` varchar(255) DEFAULT NULL COMMENT 'Gift Name',
  `critical_threshold` int(11) DEFAULT NULL COMMENT 'Critical Threshold',
  `free` int(11) DEFAULT NULL COMMENT 'Number of free gift to be given',
  `count` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created_time` int(11) DEFAULT NULL COMMENT 'Timestamp of added time',
  `updated_time` int(11) DEFAULT NULL COMMENT 'Timestamp of updated time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Critical Gift Record';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_currency`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_currency` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL DEFAULT '',
  `iso` varchar(3) NOT NULL DEFAULT '',
  `rate` double(12,4) unsigned NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1005 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_pk`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_pk` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `uid_1` int(10) unsigned NOT NULL,
  `uid_1_team` varchar(35) DEFAULT NULL,
  `uid_1_score` int(10) unsigned NOT NULL DEFAULT '0',
  `uid_2` int(10) unsigned NOT NULL,
  `uid_2_team` varchar(35) DEFAULT NULL,
  `uid_2_score` int(10) unsigned NOT NULL DEFAULT '0',
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `round` varchar(15) NOT NULL DEFAULT '',
  `ended` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100068 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_redemption`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_redemption` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `redemption_config_id` int(11) NOT NULL,
  `redemption_code_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_redemption_config_id_composite` (`uid`,`redemption_config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=696 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_redemption_code`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_redemption_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `redemption_config_id` int(11) DEFAULT NULL,
  `code` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `composite_redemption_config_id_and_status` (`redemption_config_id`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_redemption_config`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_redemption_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) DEFAULT NULL,
  `action` varchar(32) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` text,
  `has_code` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-01 16:08:11
