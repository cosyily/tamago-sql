START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(007,'UP','ilvb_fields',''); COMMIT;
ALTER TABLE `hm_contents`.`hm_channel` ADD COLUMN ilvb_stream_code varchar(45) NULL, ADD COLUMN stream_type enum('rtmp','ilvb') NOT NULL DEFAULT 'rtmp', ADD INDEX ilvb_stream_code (ilvb_stream_code);
