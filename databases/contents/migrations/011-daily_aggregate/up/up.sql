START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(011,'UP','daily_aggregate',''); COMMIT;
CREATE TABLE `daily_aggregate` (
  `event_code` int(11) unsigned  NULL,
  `room_id` int(11) unsigned NOT NULL,
  `uid` int(11) unsigned NOT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  `total` int(10) unsigned NOT NULL,
  `createdts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `daily_aggregate` ADD INDEX `idx_room_id` (`room_id`);
ALTER TABLE `daily_aggregate` ADD INDEX `idx_timestamp` (`timestamp`);
ALTER TABLE `daily_aggregate` ADD INDEX `idx_uid` (`uid`);
ALTER TABLE `daily_aggregate` ADD INDEX `idx_event_code` (`event_code`);

insert into daily_aggregate (event_code,room_id,uid,timestamp,total) select event_code,room_id,0,timestamp,sum(total) from chat_count_aggregate group by from_unixtime(timestamp + 3600 *8, '%d/%m/%Y'),room_id,event_code;

update daily_aggregate da inner join hm_channel c on da.room_id = c.room_number set da.uid=c.uid;

update daily_aggregate set uid=491646 where room_id=1243704;
update daily_aggregate set uid=211918 where room_id=1684229;
update daily_aggregate set uid=213266 where room_id=2205467;
