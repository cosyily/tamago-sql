START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(002,'UP','channel_label_tab_category',''); COMMIT;
ALTER TABLE `hm_contents`.`hm_channel_label`
   ADD COLUMN tab_display tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER rightlabeltypecat,
   ADD COLUMN category_display tinyint(1) unsigned NOT NULL DEFAULT '0';
