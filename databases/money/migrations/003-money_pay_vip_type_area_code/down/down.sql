START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(003,'DOWN','money_pay_vip_type_area_code',''); COMMIT;
ALTER TABLE `hm_money`.`money_pay_2018_8` DROP COLUMN vip_type, DROP COLUMN area_code, DROP INDEX index_area_code, DROP INDEX index_vip_type;
ALTER TABLE `hm_money`.`money_pay_2018_9` DROP COLUMN vip_type, DROP COLUMN area_code, DROP INDEX index_area_code, DROP INDEX index_vip_type;

