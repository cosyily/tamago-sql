START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(002,'DOWN','add_index',''); COMMIT;
ALTER TABLE `money_pay_2018_7` DROP INDEX `leaderboard_filter_title_ts_uid`;
ALTER TABLE `money_pay_2018_7` DROP INDEX `other_uid`;
ALTER TABLE `money_pay_2018_7` DROP INDEX `leaderboard_join_uid_other_uid_ts`;
ALTER TABLE `money_pay_2018_7` DROP INDEX `money`;

ALTER TABLE `money_pay_2018_8` DROP INDEX `leaderboard_filter_title_ts_uid`;
ALTER TABLE `money_pay_2018_8` DROP INDEX `other_uid`;
ALTER TABLE `money_pay_2018_8` DROP INDEX `leaderboard_join_uid_other_uid_ts`;
ALTER TABLE `money_pay_2018_8` DROP INDEX `money`;
