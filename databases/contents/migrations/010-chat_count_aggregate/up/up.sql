START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(010,'UP','chat_count_aggregate',''); COMMIT;
CREATE TABLE `chat_count_aggregate` (
  `event_code` int(11) unsigned NOT NULL,
  `room_id` int(11) unsigned NOT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  `total` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
