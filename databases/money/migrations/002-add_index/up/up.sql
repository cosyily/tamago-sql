START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(002,'UP','add_index',''); COMMIT;
ALTER TABLE `money_pay_2018_7` ADD INDEX `leaderboard_filter_title_ts_uid` (`title`, `ts`, `uid`);
ALTER TABLE `money_pay_2018_7` ADD INDEX `other_uid` (`other_uid`);
ALTER TABLE `money_pay_2018_7` ADD INDEX `leaderboard_join_uid_other_uid_ts` (`uid`, `other_uid`, `ts`);
ALTER TABLE `money_pay_2018_7` ADD INDEX `money` (`money`);

ALTER TABLE `money_pay_2018_8` ADD INDEX `leaderboard_filter_title_ts_uid` (`title`, `ts`, `uid`);
ALTER TABLE `money_pay_2018_8` ADD INDEX `other_uid` (`other_uid`);
ALTER TABLE `money_pay_2018_8` ADD INDEX `leaderboard_join_uid_other_uid_ts` (`uid`, `other_uid`, `ts`);
ALTER TABLE `money_pay_2018_8` ADD INDEX `money` (`money`);
