#source db config
. ./database.config

func_getdbver ()
{
    VER_IN_DB=`mysql -h $stg_host -u$stg_user -p$stg_pass -s -N -e "USE $stg_database; \. ../../src/scripts/get_db_version.sql"`
    if [ -z "$VER_IN_DB" ]; then
       VER_IN_DB=0
    fi
}



echo 'dropping staging database'
echo 'cleaning up and initializing staging database'
mysql -h $stg_host -u$stg_user -p$stg_pass -e "DROP DATABASE IF EXISTS $stg_database; CREATE DATABASE $stg_database; USE $stg_database; \. ../../src/scripts/create_tables_if_not_exists.sql"
if [ ! $? == 0 ]; then
    echo 'cannot check if tables exists in stg'
    exit 1
fi

#lock here

func_getdbver

#get repo database version
VER_IN_REPO=`ls ./migrations| sort -r | head -1 | cut -c1-3`

if [ -z "$VER_IN_REPO" ]; then
   VER_IN_REPO=0
fi
#convert 001 to 1
VER_IN_REPO=`expr $VER_IN_REPO + 0 `

echo 'current version is stg db is '$VER_IN_DB
echo 'current version in repo is '$VER_IN_REPO

#no need for down migration because staging will always go up
while [ $VER_IN_DB -ne $VER_IN_REPO ]; do
    if [ $VER_IN_DB -lt $VER_IN_REPO ]; then
        VER_TO_UPGRADE=`expr $VER_IN_DB + 1`
        SCRIPT_TO_RUN='./migrations/'`ls ./migrations| sort -r | tail -$VER_TO_UPGRADE | head -1`'/up/up.sql'
        mysql -h $stg_host -u$stg_user -p$stg_pass -e "USE $stg_database; \. $SCRIPT_TO_RUN"
        echo 'up migration to '$VER_TO_UPGRADE
        echo 'script to run '$SCRIPT_TO_RUN
        if [ ! $? == 0 ]; then
            echo 'unable to perform upgrade on version '$VER_TO_UPGRADE
            exit 1
        fi
    fi
    func_getdbver
    echo 'current version is stg db is '$VER_IN_DB
    echo 'current version in repo is '$VER_IN_REPO
done
echo 'version in stg db is same as version in repo'
#release lock here
