START TRANSACTION; insert into database_version(version,migration_type,description,reason) values(001,'UP','Initial',''); COMMIT;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: rds-prod-money-slave.tamago.db    Database: hm_money
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hm_money`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hm_money` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hm_money`;

--
-- Table structure for table `bank_card`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `admin_id` int(11) NOT NULL COMMENT '后台审核管理员账号id',
  `cid` int(10) NOT NULL COMMENT '房间号',
  `uid` int(10) NOT NULL COMMENT '房主UID',
  `card_holder` char(50) NOT NULL COMMENT '持卡人',
  `bank_name` char(50) NOT NULL COMMENT '开户行',
  `card_number` varchar(50) NOT NULL COMMENT '卡号',
  `card_id` varchar(50) NOT NULL COMMENT '身份证号',
  `bank_pic` varchar(200) NOT NULL COMMENT '银行卡正面照片',
  `card_pic` varchar(200) NOT NULL COMMENT '身份证正面照片',
  `reason` varchar(200) NOT NULL COMMENT '审核不通过原因',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1【未审核】  2【已审核】  3【审核失败】',
  `addtime` int(10) NOT NULL,
  `card_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 身份证 1 护照 2 港澳通行证 3 台胞证 4 其他',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `card_number` (`card_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='银行卡信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bank_cash`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_cash` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cid` int(10) NOT NULL COMMENT '房间短号',
  `uid` int(10) NOT NULL COMMENT '主播UID',
  `card_number` varchar(50) NOT NULL COMMENT '银行卡号',
  `bank_name` char(50) NOT NULL COMMENT '开卡银行',
  `card_holder` char(50) NOT NULL COMMENT '持卡人姓名',
  `money` decimal(10,2) NOT NULL COMMENT '分成金额',
  `money_m` decimal(10,2) NOT NULL COMMENT '此次提取的M币数量',
  `surplus` decimal(10,2) NOT NULL COMMENT '剩余M币',
  `addtime` int(10) NOT NULL COMMENT '申请时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `reason` varchar(200) NOT NULL COMMENT '原因',
  `operating` char(50) NOT NULL COMMENT '操作人',
  `order_id` varchar(50) NOT NULL COMMENT '订单号',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `status` (`status`),
  KEY `uid` (`uid`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `income_count`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `income_count` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '主播UID',
  `money` decimal(13,3) NOT NULL COMMENT '收入金额',
  `money_bef` decimal(13,3) NOT NULL,
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主播礼物收入表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `merchant_login`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merchant_login` (
  `id` int(10) unsigned NOT NULL COMMENT 'Merchants will login to merchant portal with their email to generate or view prepaid code (redeemed/not redeemed). Those expired after 1 year will not be shown.',
  `name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_0`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3400 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_0
AFTER UPDATE 
    ON money_0 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3503 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_1
AFTER UPDATE 
    ON money_1 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3268 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_10
AFTER UPDATE 
    ON money_10 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3336 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_11
AFTER UPDATE 
    ON money_11 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3420 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_12
AFTER UPDATE 
    ON money_12 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_13`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_13` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3357 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_13
AFTER UPDATE 
    ON money_13 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_14`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_14` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3385 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_14
AFTER UPDATE 
    ON money_14 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_15`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3282 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_15
AFTER UPDATE 
    ON money_15 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_16`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_16` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3350 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_16
AFTER UPDATE 
    ON money_16 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_17`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_17` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3319 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_17
AFTER UPDATE 
    ON money_17 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_18`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_18` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3506 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_18
AFTER UPDATE 
    ON money_18 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_19`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_19` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3422 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_19
AFTER UPDATE 
    ON money_19 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3431 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_2
AFTER UPDATE 
    ON money_2 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_20`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_20` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3410 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_20
AFTER UPDATE 
    ON money_20 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_21` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3357 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_22`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_22` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3445 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_23`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_23` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3352 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_24`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_24` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3296 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_25`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_25` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3264 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_26`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_26` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3363 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_27`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_27` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3239 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_27
AFTER UPDATE 
    ON money_27 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_28`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_28` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3343 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_28
AFTER UPDATE 
    ON money_28 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_29`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_29` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3365 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_29
AFTER UPDATE 
    ON money_29 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3396 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_3
AFTER UPDATE 
    ON money_3 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_30`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_30` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3240 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_30
AFTER UPDATE 
    ON money_30 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_31`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_31` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3381 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_31
AFTER UPDATE 
    ON money_31 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3375 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_4
AFTER UPDATE 
    ON money_4 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3379 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_5
AFTER UPDATE 
    ON money_5 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3351 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_6
AFTER UPDATE 
    ON money_6 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3387 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_7
AFTER UPDATE 
    ON money_7 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3436 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_8
AFTER UPDATE 
    ON money_8 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `coin` decimal(9,2) NOT NULL COMMENT '猫币',
  `bean` decimal(11,2) DEFAULT '0.00',
  `ts` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3467 DEFAULT CHARSET=utf8 COMMENT='金额表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER after_money_update_9
AFTER UPDATE 
    ON money_9 FOR EACH ROW 
BEGIN
    INSERT INTO money_log (uid,coin_from,coin_to,ts,amount) VALUES(OLD.uid,OLD.coin,NEW.coin,UNIX_TIMESTAMP(),new.coin-old.coin);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `money_anchor_salary`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_anchor_salary` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `gift_money` decimal(11,3) NOT NULL COMMENT '当月收入平台货币数量',
  `sale_giftMoney` decimal(11,3) NOT NULL COMMENT '抽成后当月货币数量',
  `guess_money` decimal(11,3) NOT NULL DEFAULT '0.000' COMMENT '竞猜收益（猫豆）',
  `sale_guessMoney` decimal(11,3) NOT NULL DEFAULT '0.000' COMMENT '打折后的竞猜收益(暂时百分百收益）',
  `month` tinyint(3) NOT NULL DEFAULT '0' COMMENT '当前月份',
  `issign` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否签约',
  `year` year(4) DEFAULT '2016' COMMENT '年份',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主播每月收入与抽成后金钱';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2000_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2000_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(10) unsigned NOT NULL COMMENT 'ä¸»æ’­ID',
  `cid` int(10) unsigned NOT NULL COMMENT 'æˆ¿é—´ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'æ”¶å…¥ç‰©å“æ•°é‡',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ç‰©å“ID',
  `money` decimal(10,2) NOT NULL COMMENT 'æ”¶å…¥å¹³å°è´§å¸æ•°é‡',
  `money_type` tinyint(2) NOT NULL COMMENT 'å¸ç§:1çŒ«å¸2çŒ«è±†',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'æ¶ˆè´¹ç±»åž‹:1é€ç¤¼',
  `ip` varchar(20) NOT NULL COMMENT 'å®¢æˆ·ç«¯IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ä»˜æ¬¾æ–¹uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ä»˜æ¬¾æ–¹æˆ¿é—´å·',
  `orderid` varchar(20) NOT NULL COMMENT 'è®¢å•å·',
  `addtime` int(10) unsigned NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  `note` varchar(50) NOT NULL COMMENT 'å¤‡æ³¨',
  `add_score` int(10) NOT NULL COMMENT 'è´¡çŒ®ç²‰ä¸å€¼',
  `nick` varchar(50) NOT NULL COMMENT 'ç”¨æˆ·æ˜µç§°',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='æˆ¿é—´æ”¶å…¥è®°å½•';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2000_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2000_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(10) unsigned NOT NULL COMMENT 'ä¸»æ’­ID',
  `cid` int(10) unsigned NOT NULL COMMENT 'æˆ¿é—´ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'æ”¶å…¥ç‰©å“æ•°é‡',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ç‰©å“ID',
  `money` decimal(10,2) NOT NULL COMMENT 'æ”¶å…¥å¹³å°è´§å¸æ•°é‡',
  `money_type` tinyint(2) NOT NULL COMMENT 'å¸ç§:1çŒ«å¸2çŒ«è±†',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'æ¶ˆè´¹ç±»åž‹:1é€ç¤¼',
  `ip` varchar(20) NOT NULL COMMENT 'å®¢æˆ·ç«¯IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ä»˜æ¬¾æ–¹uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ä»˜æ¬¾æ–¹æˆ¿é—´å·',
  `orderid` varchar(20) NOT NULL COMMENT 'è®¢å•å·',
  `addtime` int(10) unsigned NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  `note` varchar(50) NOT NULL COMMENT 'å¤‡æ³¨',
  `add_score` int(10) NOT NULL COMMENT 'è´¡çŒ®ç²‰ä¸å€¼',
  `nick` varchar(50) NOT NULL COMMENT 'ç”¨æˆ·æ˜µç§°',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='æˆ¿é—´æ”¶å…¥è®°å½•';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2015_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2015_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(10) unsigned NOT NULL COMMENT 'ä¸»æ’­ID',
  `cid` int(10) unsigned NOT NULL COMMENT 'æˆ¿é—´ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'æ”¶å…¥ç‰©å“æ•°é‡',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ç‰©å“ID',
  `money` decimal(10,2) NOT NULL COMMENT 'æ”¶å…¥å¹³å°è´§å¸æ•°é‡',
  `money_type` tinyint(2) NOT NULL COMMENT 'å¸ç§:1çŒ«å¸2çŒ«è±†',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'æ¶ˆè´¹ç±»åž‹:1é€ç¤¼',
  `ip` varchar(20) NOT NULL COMMENT 'å®¢æˆ·ç«¯IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ä»˜æ¬¾æ–¹uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ä»˜æ¬¾æ–¹æˆ¿é—´å·',
  `orderid` varchar(20) NOT NULL COMMENT 'è®¢å•å·',
  `addtime` int(10) unsigned NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  `note` varchar(50) NOT NULL COMMENT 'å¤‡æ³¨',
  `add_score` int(10) NOT NULL COMMENT 'è´¡çŒ®ç²‰ä¸å€¼',
  `nick` varchar(50) NOT NULL COMMENT 'ç”¨æˆ·æ˜µç§°',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='æˆ¿é—´æ”¶å…¥è®°å½•';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2015_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2015_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(10) unsigned NOT NULL COMMENT 'ä¸»æ’­ID',
  `cid` int(10) unsigned NOT NULL COMMENT 'æˆ¿é—´ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'æ”¶å…¥ç‰©å“æ•°é‡',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ç‰©å“ID',
  `money` decimal(10,2) NOT NULL COMMENT 'æ”¶å…¥å¹³å°è´§å¸æ•°é‡',
  `money_type` tinyint(2) NOT NULL COMMENT 'å¸ç§:1çŒ«å¸2çŒ«è±†',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'æ¶ˆè´¹ç±»åž‹:1é€ç¤¼',
  `ip` varchar(20) NOT NULL COMMENT 'å®¢æˆ·ç«¯IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ä»˜æ¬¾æ–¹uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ä»˜æ¬¾æ–¹æˆ¿é—´å·',
  `orderid` varchar(20) NOT NULL COMMENT 'è®¢å•å·',
  `addtime` int(10) unsigned NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  `note` varchar(50) NOT NULL COMMENT 'å¤‡æ³¨',
  `add_score` int(10) NOT NULL COMMENT 'è´¡çŒ®ç²‰ä¸å€¼',
  `nick` varchar(50) NOT NULL COMMENT 'ç”¨æˆ·æ˜µç§°',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='æˆ¿é—´æ”¶å…¥è®°å½•';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2015_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2015_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(10) unsigned NOT NULL COMMENT 'ä¸»æ’­ID',
  `cid` int(10) unsigned NOT NULL COMMENT 'æˆ¿é—´ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'æ”¶å…¥ç‰©å“æ•°é‡',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ç‰©å“ID',
  `money` decimal(10,2) NOT NULL COMMENT 'æ”¶å…¥å¹³å°è´§å¸æ•°é‡',
  `money_type` tinyint(2) NOT NULL COMMENT 'å¸ç§:1çŒ«å¸2çŒ«è±†',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'æ¶ˆè´¹ç±»åž‹:1é€ç¤¼',
  `ip` varchar(20) NOT NULL COMMENT 'å®¢æˆ·ç«¯IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ä»˜æ¬¾æ–¹uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ä»˜æ¬¾æ–¹æˆ¿é—´å·',
  `orderid` varchar(20) NOT NULL COMMENT 'è®¢å•å·',
  `addtime` int(10) unsigned NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  `note` varchar(50) NOT NULL COMMENT 'å¤‡æ³¨',
  `add_score` int(10) NOT NULL COMMENT 'è´¡çŒ®ç²‰ä¸å€¼',
  `nick` varchar(50) NOT NULL COMMENT 'ç”¨æˆ·æ˜µç§°',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='æˆ¿é—´æ”¶å…¥è®°å½•';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2016_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2016_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=318950 DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=10524 DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2017_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2018_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2018_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=32109 DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2018_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2018_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=34814 DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2018_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2018_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=42166 DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2018_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2018_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=47539 DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2018_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2018_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=60085 DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2018_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2018_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型:1送礼,2竞猜-坐庄,3竞猜-下注',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=83427 DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2018_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2018_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型: 1. 送礼(Give gift), 2. 竞猜-坐庄(Quiz banker), 3. 竞猜-下注(Answering quiz question), 4. Colour chat',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=135697 DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_channel_income_2018_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_channel_income_2018_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(10) unsigned NOT NULL COMMENT '主播ID',
  `cid` int(10) unsigned NOT NULL COMMENT '房间ID',
  `item_acount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '收入物品数量',
  `item` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `money` decimal(10,2) NOT NULL COMMENT '收入平台货币数量',
  `money_type` tinyint(2) NOT NULL COMMENT '币种:1猫币2猫豆',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `pay_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '消费类型: 1. 送礼(Give gift), 2. 竞猜-坐庄(Quiz banker), 3. 竞猜-下注(Answering quiz question), 4. Colour chat',
  `ip` varchar(20) NOT NULL COMMENT '客户端IP',
  `other_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方uid',
  `other_cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付款方房间号',
  `orderid` varchar(20) NOT NULL COMMENT '订单号',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `note` varchar(50) NOT NULL COMMENT '备注',
  `add_score` int(10) NOT NULL COMMENT '贡献粉丝值',
  `nick` varchar(50) NOT NULL COMMENT '用户昵称',
  PRIMARY KEY (`id`),
  KEY `addtime` (`addtime`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2940 DEFAULT CHARSET=utf8 COMMENT='房间收入记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2016_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2016_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2017_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2018_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2018_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2018_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2018_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2018_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2018_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2018_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2018_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2018_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2018_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2018_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2018_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2018_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2018_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_lierenshoprecharge_2018_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_lierenshoprecharge_2018_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1猫豆 2猫币 ',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `user_id` char(20) NOT NULL COMMENT '猎人的用户标示',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `lierenaccount` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lierenaccountindex` (`lierenaccount`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_log` (
  `uid` bigint(20) DEFAULT NULL,
  `coin_from` decimal(9,2) DEFAULT NULL,
  `coin_to` decimal(9,2) DEFAULT NULL,
  `ts` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  KEY `money_log_uid` (`uid`),
  KEY `money_log_ts` (`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2000_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2000_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(11) NOT NULL COMMENT 'ç”¨æˆ·UID',
  `acount` varchar(20) NOT NULL COMMENT 'æ¶ˆè´¹è®¢å•å·',
  `title` varchar(50) NOT NULL COMMENT 'æ ‡é¢˜',
  `money_type` tinyint(2) NOT NULL COMMENT 'æ¶ˆè´¹å¸ç§ 1çŒ«å¸2 çŒ«è±† 3ä»™è±†',
  `pay_type` tinyint(3) NOT NULL COMMENT 'æ¶ˆè´¹ç±»åž‹',
  `pay_num` int(9) NOT NULL COMMENT 'æ¶ˆè´¹æ•°é‡',
  `money` int(12) NOT NULL COMMENT 'æ¶ˆè´¹é‡‘é¢',
  `other_uid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹uid',
  `other_cid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹æˆ¿é—´å·',
  `ts` int(12) NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ç”¨æˆ·æ¶ˆè´¹è¡¨';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2000_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2000_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(11) NOT NULL COMMENT 'ç”¨æˆ·UID',
  `acount` varchar(20) NOT NULL COMMENT 'æ¶ˆè´¹è®¢å•å·',
  `title` varchar(50) NOT NULL COMMENT 'æ ‡é¢˜',
  `money_type` tinyint(2) NOT NULL COMMENT 'æ¶ˆè´¹å¸ç§ 1çŒ«å¸2 çŒ«è±† 3ä»™è±†',
  `pay_type` tinyint(3) NOT NULL COMMENT 'æ¶ˆè´¹ç±»åž‹',
  `pay_num` int(9) NOT NULL COMMENT 'æ¶ˆè´¹æ•°é‡',
  `money` int(12) NOT NULL COMMENT 'æ¶ˆè´¹é‡‘é¢',
  `other_uid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹uid',
  `other_cid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹æˆ¿é—´å·',
  `ts` int(12) NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ç”¨æˆ·æ¶ˆè´¹è¡¨';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2014_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2014_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(11) NOT NULL COMMENT 'ç”¨æˆ·UID',
  `acount` varchar(20) NOT NULL COMMENT 'æ¶ˆè´¹è®¢å•å·',
  `title` varchar(50) NOT NULL COMMENT 'æ ‡é¢˜',
  `money_type` tinyint(2) NOT NULL COMMENT 'æ¶ˆè´¹å¸ç§ 1çŒ«å¸2 çŒ«è±† 3ä»™è±†',
  `pay_type` tinyint(3) NOT NULL COMMENT 'æ¶ˆè´¹ç±»åž‹',
  `pay_num` int(9) NOT NULL COMMENT 'æ¶ˆè´¹æ•°é‡',
  `money` int(12) NOT NULL COMMENT 'æ¶ˆè´¹é‡‘é¢',
  `other_uid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹uid',
  `other_cid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹æˆ¿é—´å·',
  `ts` int(12) NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ç”¨æˆ·æ¶ˆè´¹è¡¨';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2015_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2015_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(11) NOT NULL COMMENT 'ç”¨æˆ·UID',
  `acount` varchar(20) NOT NULL COMMENT 'æ¶ˆè´¹è®¢å•å·',
  `title` varchar(50) NOT NULL COMMENT 'æ ‡é¢˜',
  `money_type` tinyint(2) NOT NULL COMMENT 'æ¶ˆè´¹å¸ç§ 1çŒ«å¸2 çŒ«è±† 3ä»™è±†',
  `pay_type` tinyint(3) NOT NULL COMMENT 'æ¶ˆè´¹ç±»åž‹',
  `pay_num` int(9) NOT NULL COMMENT 'æ¶ˆè´¹æ•°é‡',
  `money` int(12) NOT NULL COMMENT 'æ¶ˆè´¹é‡‘é¢',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹uid',
  `other_cid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹æˆ¿é—´å·',
  `ts` int(12) NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ç”¨æˆ·æ¶ˆè´¹è¡¨';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2015_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2015_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(11) NOT NULL COMMENT 'ç”¨æˆ·UID',
  `acount` varchar(20) NOT NULL COMMENT 'æ¶ˆè´¹è®¢å•å·',
  `title` varchar(50) NOT NULL COMMENT 'æ ‡é¢˜',
  `money_type` tinyint(2) NOT NULL COMMENT 'æ¶ˆè´¹å¸ç§ 1çŒ«å¸2 çŒ«è±† 3ä»™è±†',
  `pay_type` tinyint(3) NOT NULL COMMENT 'æ¶ˆè´¹ç±»åž‹',
  `pay_num` int(9) NOT NULL COMMENT 'æ¶ˆè´¹æ•°é‡',
  `money` int(12) NOT NULL COMMENT 'æ¶ˆè´¹é‡‘é¢',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹uid',
  `other_cid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹æˆ¿é—´å·',
  `ts` int(12) NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ç”¨æˆ·æ¶ˆè´¹è¡¨';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2015_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2015_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(11) NOT NULL COMMENT 'ç”¨æˆ·UID',
  `acount` varchar(20) NOT NULL COMMENT 'æ¶ˆè´¹è®¢å•å·',
  `title` varchar(50) NOT NULL COMMENT 'æ ‡é¢˜',
  `money_type` tinyint(2) NOT NULL COMMENT 'æ¶ˆè´¹å¸ç§ 1çŒ«å¸2 çŒ«è±† 3ä»™è±†',
  `pay_type` tinyint(3) NOT NULL COMMENT 'æ¶ˆè´¹ç±»åž‹',
  `pay_num` int(9) NOT NULL COMMENT 'æ¶ˆè´¹æ•°é‡',
  `money` int(12) NOT NULL COMMENT 'æ¶ˆè´¹é‡‘é¢',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹uid',
  `other_cid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹æˆ¿é—´å·',
  `ts` int(12) NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ç”¨æˆ·æ¶ˆè´¹è¡¨';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2015_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2015_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(11) NOT NULL COMMENT 'ç”¨æˆ·UID',
  `acount` varchar(20) NOT NULL COMMENT 'æ¶ˆè´¹è®¢å•å·',
  `title` varchar(50) NOT NULL COMMENT 'æ ‡é¢˜',
  `money_type` tinyint(2) NOT NULL COMMENT 'æ¶ˆè´¹å¸ç§ 1çŒ«å¸2 çŒ«è±† 3ä»™è±†',
  `pay_type` tinyint(3) NOT NULL COMMENT 'æ¶ˆè´¹ç±»åž‹',
  `pay_num` int(9) NOT NULL COMMENT 'æ¶ˆè´¹æ•°é‡',
  `money` int(12) NOT NULL COMMENT 'æ¶ˆè´¹é‡‘é¢',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹uid',
  `other_cid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹æˆ¿é—´å·',
  `ts` int(12) NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ç”¨æˆ·æ¶ˆè´¹è¡¨';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2015_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2015_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(11) NOT NULL COMMENT 'ç”¨æˆ·UID',
  `acount` varchar(20) NOT NULL COMMENT 'æ¶ˆè´¹è®¢å•å·',
  `title` varchar(50) NOT NULL COMMENT 'æ ‡é¢˜',
  `money_type` tinyint(2) NOT NULL COMMENT 'æ¶ˆè´¹å¸ç§ 1çŒ«å¸2 çŒ«è±† 3ä»™è±†',
  `pay_type` tinyint(3) NOT NULL COMMENT 'æ¶ˆè´¹ç±»åž‹',
  `pay_num` int(9) NOT NULL COMMENT 'æ¶ˆè´¹æ•°é‡',
  `money` int(12) NOT NULL COMMENT 'æ¶ˆè´¹é‡‘é¢',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹uid',
  `other_cid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹æˆ¿é—´å·',
  `ts` int(12) NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ç”¨æˆ·æ¶ˆè´¹è¡¨';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2015_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2015_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ä¸»é”®ID',
  `uid` int(11) NOT NULL COMMENT 'ç”¨æˆ·UID',
  `acount` varchar(20) NOT NULL COMMENT 'æ¶ˆè´¹è®¢å•å·',
  `title` varchar(50) NOT NULL COMMENT 'æ ‡é¢˜',
  `money_type` tinyint(2) NOT NULL COMMENT 'æ¶ˆè´¹å¸ç§ 1çŒ«å¸2 çŒ«è±† 3ä»™è±†',
  `pay_type` tinyint(3) NOT NULL COMMENT 'æ¶ˆè´¹ç±»åž‹',
  `pay_num` int(9) NOT NULL COMMENT 'æ¶ˆè´¹æ•°é‡',
  `money` int(12) NOT NULL COMMENT 'æ¶ˆè´¹é‡‘é¢',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹uid',
  `other_cid` int(11) DEFAULT NULL COMMENT 'äº¤æ˜“æ–¹æˆ¿é—´å·',
  `ts` int(12) NOT NULL COMMENT 'æ·»åŠ æ—¶é—´',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ç”¨æˆ·æ¶ˆè´¹è¡¨';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2016_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2016_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7背包 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7弹幕卡 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB AUTO_INCREMENT=256185 DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7弹幕卡 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB AUTO_INCREMENT=10249 DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_5_bak`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_5_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7背包 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` int(12) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2017_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7背包 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2018_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2018_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7弹幕卡 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB AUTO_INCREMENT=31837 DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2018_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2018_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7弹幕卡 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB AUTO_INCREMENT=33841 DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2018_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2018_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7弹幕卡 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB AUTO_INCREMENT=39171 DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2018_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2018_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7弹幕卡 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB AUTO_INCREMENT=45820 DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2018_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2018_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7弹幕卡 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB AUTO_INCREMENT=57675 DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2018_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2018_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7弹幕卡 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB AUTO_INCREMENT=81027 DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2018_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2018_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7弹幕卡 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB AUTO_INCREMENT=108890 DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_pay_2018_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_pay_2018_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(20) NOT NULL COMMENT '消费订单号',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `money_type` tinyint(2) NOT NULL COMMENT '消费币种 1猫币2 猫豆 3仙豆',
  `pay_type` tinyint(3) NOT NULL COMMENT '消费类型  2竞猜坐庄 3竞猜下注 4礼物消费 5猎人消费 6弹幕消费 7弹幕卡 8守护...',
  `pay_num` int(9) NOT NULL COMMENT '消费数量',
  `money` decimal(10,2) NOT NULL COMMENT '消费金额',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT '0:正常用户，1:白名单用户',
  `other_uid` int(11) DEFAULT NULL COMMENT '交易方uid',
  `other_cid` int(11) DEFAULT NULL COMMENT '交易方房间号',
  `ts` int(12) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `acount` (`acount`)
) ENGINE=InnoDB AUTO_INCREMENT=1707 DEFAULT CHARSET=utf8 COMMENT='用户消费表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_1` (
  `id` int(11) NOT NULL COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2微信 3qqgame、4apple 5 ipad',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` int(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` int(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL,
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3..',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3..',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3..',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_2` (
  `id` int(11) NOT NULL COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2微信 3qqgame、4apple 5 ipad',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` int(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` int(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL,
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_3` (
  `id` int(11) NOT NULL COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2微信 3qqgame、4apple 5 ipad',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` int(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` int(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL,
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_4` (
  `id` int(11) NOT NULL COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2微信 3qqgame、4apple 5 ipad',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` int(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` int(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL,
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_5` (
  `id` int(11) NOT NULL COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2微信 3qqgame、4apple 5 ipad',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` int(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` int(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL,
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2微信 3qqgame、4apple 5 ipad',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` int(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` int(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL,
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2微信 3qqgame、4apple 5 ipad',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` int(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` int(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL,
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3..',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2016_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2016_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3..',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_10`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_11`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_11` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=54217 DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_12`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_12` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2093 DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2017_9`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2017_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2018_1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2018_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=7140 DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2018_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2018_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=7687 DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2018_3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2018_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=10552 DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2018_4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2018_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(30) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(12) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=9410 DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2018_5`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2018_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(36) NOT NULL DEFAULT '' COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(20) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(16) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=10211 DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2018_6`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2018_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(36) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(20) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=12148 DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2018_7`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2018_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(36) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(20) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=14101 DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_recharge_2018_8`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_recharge_2018_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `uid` int(11) NOT NULL COMMENT '用户UID',
  `acount` varchar(36) NOT NULL COMMENT '订单号',
  `title` varchar(60) NOT NULL COMMENT '订单标题',
  `content` varchar(120) NOT NULL COMMENT '订单描述',
  `pay_money` varchar(20) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '订单状态1：预支付；2：支付完成',
  `money` varchar(12) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `pay_mode` tinyint(2) NOT NULL COMMENT '支付类型 1支付宝 2网银 3.. 6快钱,11娱乐支付',
  `mode` varchar(20) NOT NULL COMMENT '交易方',
  `type` tinyint(2) NOT NULL COMMENT '交易类型；已经废弃',
  `createts` varchar(12) NOT NULL COMMENT '订单创建时间',
  `recharge_user` varchar(50) NOT NULL COMMENT '交易方支付账号',
  `updatets` varchar(12) NOT NULL COMMENT '订单完成时间',
  `clientip` varchar(20) NOT NULL COMMENT '客户端IP',
  `source` varchar(20) NOT NULL COMMENT '来源：android，IOS，PC，IPAD',
  `note` text COMMENT '快钱字段集合',
  `lm_acount` char(50) DEFAULT NULL COMMENT '乐米的订单号',
  `lm_uid` char(50) DEFAULT NULL COMMENT '乐米用户id',
  `lm_status` tinyint(1) DEFAULT '1' COMMENT '订单状态1：未支付；2：已支付未结算；3：已支付已结算',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `updatets` (`updatets`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COMMENT='充值记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `money_topup`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `money_topup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(10) NOT NULL,
  `refno` varchar(16) DEFAULT NULL,
  `transactionid` varchar(45) DEFAULT NULL,
  `status` int(2) NOT NULL COMMENT '0 pending, 1 paid, 2 processed, 3 refunded, 4 is error in payment',
  `tcoin` varchar(20) NOT NULL,
  `money` varchar(20) NOT NULL,
  `type` tinyint(2) DEFAULT NULL COMMENT '1 -paypal, 2- adyen',
  `createdts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `paidts` timestamp NULL DEFAULT NULL,
  `processedts` timestamp NULL DEFAULT NULL,
  `paypalid` varchar(45) DEFAULT NULL,
  `orderid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prepaid_topup`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prepaid_topup` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `code` char(64) DEFAULT NULL,
  `validity` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - Not yet used\n1 - Used\n2 - Expired',
  `validitydate` date NOT NULL,
  `topupvalue` int(11) NOT NULL,
  `topupmoney` varchar(20) NOT NULL,
  `redeemts` datetime DEFAULT NULL,
  `createdts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `phoneno` varchar(15) DEFAULT NULL,
  `areacode` varchar(5) DEFAULT NULL,
  `refno` varchar(32) DEFAULT NULL,
  `merchant_id` int(10) NOT NULL,
  `hash_zone` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`,`hash_zone`),
  UNIQUE KEY `PrepaidTopUpcol_UNIQUE` (`code`,`hash_zone`)
) ENGINE=InnoDB AUTO_INCREMENT=856 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_android_purchase`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_android_purchase` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `gpa` varchar(24) NOT NULL DEFAULT '',
  `token` varchar(255) NOT NULL DEFAULT '',
  `purchase_state` tinyint(4) NOT NULL,
  `package_name` varchar(35) NOT NULL DEFAULT '',
  `product_id` varchar(35) NOT NULL DEFAULT '',
  `currency` varchar(3) NOT NULL,
  `amount_micro` float unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gpa` (`gpa`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmg_box_cost`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmg_box_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT 'ID of the user who give this gift',
  `channel_id` int(11) DEFAULT NULL COMMENT 'ID of the channel',
  `room_id` int(11) DEFAULT NULL COMMENT 'ID of the anchor',
  `order_id` varchar(45) DEFAULT NULL COMMENT 'ID of the order',
  `gift_id` int(11) DEFAULT NULL COMMENT 'ID of the gift',
  `gift_name` varchar(255) DEFAULT NULL COMMENT 'Name of the gift',
  `gift_cost` int(11) DEFAULT NULL COMMENT 'Cost of the gift in t coin',
  `quantity` int(11) DEFAULT NULL COMMENT 'Quantity of the gift in this box',
  `tcoin_cost` int(11) DEFAULT NULL COMMENT 'Total cost of this box',
  `ip` varchar(20) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL COMMENT 'Timestamp of the added time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9858 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `topup_payment`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topup_payment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gateway` tinyint(3) NOT NULL COMMENT '1 = Paypal\n2 = Adyen',
  `invoice` varchar(36) NOT NULL COMMENT 'Invoice number (uuid)',
  `uid` int(11) NOT NULL,
  `sku` varchar(36) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `currency` char(3) NOT NULL,
  `tcoins` int(11) NOT NULL,
  `payer_id` varchar(45) DEFAULT NULL COMMENT 'Payment gateway payer_id (like PayPal payer_id)',
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `payer_email` varchar(45) DEFAULT NULL COMMENT 'Payer?s email',
  `txn_id` varchar(36) DEFAULT NULL COMMENT 'IPN used, if Adyen it would store pspReference of the NotificationRequestItem and PayPal would be txn_id',
  `payment_type` varchar(16) DEFAULT NULL COMMENT 'IPN used. Kind of payment(eg Paypal payment_type instant). If adyen it would be its paymentMethod (visa & etc)',
  `payment_status` varchar(45) NOT NULL COMMENT 'IPN used. State of the payment(Paypal created, approved, failed) and IPN payment_status = Completed',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `country_code` char(2) NOT NULL COMMENT 'two-character ISO 3166-1 code ',
  `created_ip` varchar(39) NOT NULL,
  `updated_ip` varchar(39) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8
/*!50100 PARTITION BY HASH (id)
PARTITIONS 10 */;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-01 16:17:07
